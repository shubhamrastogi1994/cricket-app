<?php
return [
    'name'=> 'Cricket App',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'urlManager' => [
            'enablePrettyUrl' => false,
            'showScriptName' => false,
            'rules' => [
                'gii/<controller:[a-z-]+>/<action:[a-z-]+>' => 'gii/<controller>/<action>'
            ],
        ],
    ],
];
