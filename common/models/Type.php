<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_type".
 *
 * @property int $int_type_id
 * @property int $int_category_id
 * @property int $int_type
 * @property string $txt_name
 * @property string $dat_created
 * @property string $created_by
 * @property string $dat_modified
 * @property string $modified_by
 */
class Type extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['int_type_id'], 'required'],
            [['int_type_id', 'int_category_id', 'int_type'], 'integer'],
            [['dat_created', 'dat_modified'], 'safe'],
            [['txt_name'], 'string', 'max' => 100],
            [['created_by', 'modified_by'], 'string', 'max' => 45],
            [['int_type_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'int_type_id' => Yii::t('app', 'Type ID'),
            'int_category_id' => Yii::t('app', 'Category ID'),
            'int_type' => Yii::t('app', 'Type'),
            'txt_name' => Yii::t('app', 'Name'),
            'dat_created' => Yii::t('app', 'Dat Created'),
            'created_by' => Yii::t('app', 'Created By'),
            'dat_modified' => Yii::t('app', 'Dat Modified'),
            'modified_by' => Yii::t('app', 'Modfied By'),
        ];
    }
}
