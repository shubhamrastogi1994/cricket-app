<?php

namespace common\models;


use kartik\date\DatePicker;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "city".
 *
 * @property string $dat_created
 * @property int $created_by
 * @property string $dat_modified
 * @property int $modified_by
 *
 * @property User $createdBy
 * @property User $updatedBy
 * @property State $intState
 * @property Country $intCountry
 */
class BaseModel extends \yii\db\ActiveRecord
{

    const ACTIVE_LABEL_HTML = '<span class="label label-success">Active</span>';
    const INACTIVE_LABEL_HTML = '<span class="label label-danger">Deactive</span>';
    const YES_LABEL_HTML = '<span class="label label-success">Yes</span>';
    const NO_LABEL_HTML = '<span class="label label-danger">No</span>';
    const DATE_DB_FORMAT = 'php:Y-m-d H:i:s';
    const DATE_FORMAT_VIEW = 'php:d-M-Y';
    const MAX_BUTTON_COUNT_PAGINATION = 5;
    const TEMPLATE_GRID = '<div class="container p-2"><div class="row"><div class="col-12 p-0 m-0 table-responsive">{summary}{items}<div class="text-center">{pager}</div></div></div>';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $arrColumns = array_flip($this->getTableSchema()->columnNames);
        $arrRules = [];
        if (array_key_exists('created_by', $arrColumns)) {
            $arrRules['created_by'] = [['created_by'], 'integer'];
            $arrRules['created_by_exists'] = [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']];
        }
        if (array_key_exists('modified_by', $arrColumns)) {
            $arrRules['modified_by'] = [['modified_by'], 'integer'];
            $arrRules['modified_by_exists'] = [['modified_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['modified_by' => 'id']];
        }
        if (array_key_exists('dat_created', $arrColumns)) {
            $arrRules['dat_created'] = [['dat_created'], 'date', 'format' => self::DATE_DB_FORMAT];
        }
        if (array_key_exists('dat_modified', $arrColumns)) {
            $arrRules['dat_modified'] = [['dat_modified'], 'date', 'format' => self::DATE_DB_FORMAT];
        }

        if (array_key_exists('int_org_id', $arrColumns)) {
            $arrRules['int_org_id'] = [['int_org_id'], 'integer'];
            $arrRules['int_org_id_exists'] = [['int_org_id'], 'exist', 'skipOnError' => true, 'targetClass' => Organization::className(), 'targetAttribute' => ['int_org_id' => 'int_org_id']];
        }
        if (array_key_exists('int_deleted', $arrColumns)) {
            $arrRules['int_deleted'] = [['int_deleted'], 'integer'];
        }
        return ArrayHelper::merge(parent::rules(), $arrRules);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dat_created' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'dat_modified' => Yii::t('app', 'Updated At'),
            'modified_by' => Yii::t('app', 'Updated By'),
            'started_at' => Yii::t('app', 'Start Date'),
            'ended_at' => Yii::t('app', 'End Date'),
            'int_deleted' => Yii::t('app', 'Deleted'),
            'name' => Yii::t('app', 'Name'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }

    /**
     * @inheritdoc
     */

    public function beforeValidate()
    {
        $arrCols = ArrayHelper::getColumn($this->getTableSchema()->columns, 'dbType');

        foreach ($arrCols as $colKey => $colType) {
            if ($colType == 'date' && !empty($this->$colKey)) {
                $this->$colKey = Yii::$app->formatter->asDate($this->$colKey, 'php:Y-m-d');
            }
            if ($colType == 'datetime' && !empty($this->$colKey)) {
                $this->$colKey = Yii::$app->formatter->asDatetime($this->$colKey, 'php:Y-m-d H:i:s');
            }
            if ($colType == 'time' && !empty($this->$colKey)) {
                $this->$colKey = Yii::$app->formatter->asTime($this->$colKey, 'php:H:i:s');
            }
        }

        return parent::beforeValidate();
    }


    public function beforeSave($insert)
    {
        $arrCols = ArrayHelper::getColumn($this->getTableSchema()->columns, 'dbType');
        if ($this->isNewRecord) {
            if (array_key_exists('dat_created', $arrCols)) {
                $this->dat_created = Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
            }
            if (array_key_exists('created_by', $arrCols) && !Yii::$app->user->isGuest) {
                $this->created_by = Yii::$app->user->identity->id;
            }

            if (array_key_exists('created_by', $arrCols) && Yii::$app->user->isGuest) {
                $this->created_by = 1;
            }
        } else {
            if (array_key_exists('dat_modified', $arrCols)) {
                $this->dat_modified = Yii::$app->formatter->asDatetime('now', 'php:Y-m-d H:i:s');
            }
            if (array_key_exists('modified_by', $arrCols) && !Yii::$app->user->isGuest) {
                $this->modified_by = Yii::$app->user->identity->id;
            }
            if (array_key_exists('modified_by', $arrCols) && Yii::$app->user->isGuest) {
                $this->modified_by = 1;
            }
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function handleDate($model)
    {
        $arrCols = ArrayHelper::getColumn($model->getTableSchema()->columns, 'dbType');
        foreach ($arrCols as $colKey => $colType) {
            if ($colType == 'date' && !empty($model->$colKey)) {
                $model->$colKey = Yii::$app->formatter->asDate($model->$colKey, 'php:d-M-Y');
            }
            if ($colType == 'datetime' && !empty($model->$colKey)) {
                $model->$colKey = Yii::$app->formatter->asDate($model->$colKey, 'php:d-M-Y');
            }
            if ($colType == 'time' && !empty($this->$colKey)) {
                $this->$colKey = Yii::$app->formatter->asTime($this->$colKey, 'php:h:i a');

            }

        }
//        exit;
        return $model;
    }


    public static function getDaysOfWeek($intType = '')
    {
        $arrWeekDays = [
            1 => 'Monday',
            2 => 'Tuesday',
            3 => 'Wednesday',
            4 => 'Thursday',
            5 => 'Friday',
            6 => 'Saturday',
            7 => 'Sunday',
        ];
        if ($intType) {
            return (!empty($arrWeekDays[$intType]) ? $arrWeekDays[$intType] : '');
        }
        return $arrWeekDays;
    }

    public function getDatePickerOptions($arrOptions = [])
    {
        return ArrayHelper::merge($arrOptions, [
            'type' => DatePicker::TYPE_COMPONENT_APPEND,
            'pluginOptions' => [
                'autoclose' => true,
                'todayHighlight' => true,
                'format' => 'dd-M-yyyy'
            ]
        ]);
    }


}
