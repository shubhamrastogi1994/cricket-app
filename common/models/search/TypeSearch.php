<?php

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Type as TypeModel;

/**
 * Type represents the model behind the search form of `common\models\Type`.
 */
class TypeSearch extends TypeModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['int_type_id', 'int_category_id', 'int_type'], 'integer'],
            [['txt_name',], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TypeModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'int_type_id' => $this->int_type_id,
            'int_category_id' => $this->int_category_id,
            'int_type' => $this->int_type,
            'dat_created' => $this->dat_created,
            'dat_modified' => $this->dat_modified,
        ]);

        $query->andFilterWhere(['like', 'txt_name', $this->txt_name]);
          
        return $dataProvider;
    }
}
