<?php

use yii\db\Migration;

/**
 * Class m191008_132730_all_tables
 */
class m191008_132730_all_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
$strQuery =<<<SQL
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- ------------------------------------------------------
-- Server version	5.7.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

INSERT INTO `user` (`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`created_at`,`updated_at`,`verification_token`) VALUES (1,'admin','Rm-I00FMUlDtTcWWWh4RA9f3TOaqbluw','$2y$13\$ns88RKNgu3OJaCOCCAibM.T9MaVhB2wJGhKy4M6/FQmWgH7UB8F9m',NULL,'admin@outlook.in',10,1570346850,1570346850,'cBAkFUmBq1XIVhWLAyE6yFij2LKCOk4t_1570346850');

--
-- Table structure for table `tbl_category`
--

DROP TABLE IF EXISTS `tbl_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_category` (
  `int_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `int_category_type_id` int(11) DEFAULT NULL,
  `int_type_id` int(11) DEFAULT NULL,
  `txt_name` varchar(100) DEFAULT NULL,
  `dat_created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `dat_modified` datetime DEFAULT NULL,
  `modfied_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`int_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_category`
--

LOCK TABLES `tbl_category` WRITE;
/*!40000 ALTER TABLE `tbl_category` DISABLE KEYS */;
INSERT INTO `tbl_category` VALUES (1,1,NULL,'Batting Style','2019-10-06 12:28:00',1,NULL,NULL),(2,2,NULL,'Balling Style','2019-10-06 12:28:13',1,NULL,NULL),(3,1,1,'Left Hand Bat','2019-10-06 17:38:10',1,NULL,NULL),(4,2,1,'Right-arm Medium','2019-10-06 17:40:23',1,NULL,NULL);
/*!40000 ALTER TABLE `tbl_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_country`
--

DROP TABLE IF EXISTS `tbl_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_country` (
  `int_country_id` int(11) NOT NULL AUTO_INCREMENT,
  `txt_name` varchar(100) DEFAULT NULL,
  `txt_short_name` varchar(25) DEFAULT NULL,
  `txt_code` varchar(10) DEFAULT NULL,
  `dat_created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `dat_modified` datetime DEFAULT NULL,
  `modfied_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`int_country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_country`
--

LOCK TABLES `tbl_country` WRITE;
/*!40000 ALTER TABLE `tbl_country` DISABLE KEYS */;
INSERT INTO `tbl_country` VALUES (8,'Sri Lanka','SL','',NULL,1,NULL,NULL),(9,'India','IND','','2019-10-06 16:34:20',1,NULL,NULL),(10,'Australia','AUS','','2019-10-06 16:34:38',1,NULL,NULL),(12,'England','ENG','','2019-10-07 15:14:59',1,NULL,NULL),(13,'West Indies','Wi','','2019-10-07 15:15:08',1,NULL,NULL),(14,'South Africa','SA','','2019-10-07 15:15:20',1,NULL,NULL),(15,'Pakistan','PAK','','2019-10-07 15:17:13',1,NULL,NULL),(16,'New Zealand','NAZ','','2019-10-07 15:17:23',1,NULL,NULL),(17,'Bangladesh','BAN','','2019-10-07 15:17:50',1,NULL,NULL),(18,'Afghanistan','AFG','','2019-10-07 15:17:59',1,NULL,NULL);
/*!40000 ALTER TABLE `tbl_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_match`
--

DROP TABLE IF EXISTS `tbl_match`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_match` (
  `int_match_id` int(11) NOT NULL AUTO_INCREMENT,
  `int_team1_id` int(11) NOT NULL,
  `int_team2_id` int(11) NOT NULL,
  `dat_match` date DEFAULT NULL,
  `int_venue_id` int(11) NOT NULL,
  `int_match_season_id` int(11) NOT NULL,
  `int_toss_winner_team_id` int(11) DEFAULT NULL,
  `int_toss_decision_type` int(11) DEFAULT NULL,
  `int_win_type` int(11) DEFAULT NULL,
  `int_win_margin` int(11) DEFAULT NULL,
  `int_player_of_the_match_id` int(11) DEFAULT NULL,
  `int_winner_team_id` int(11) DEFAULT NULL,
  `dat_created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `dat_modified` datetime DEFAULT NULL,
  `modfied_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`int_match_id`),
  KEY `fk_match_team1` (`int_team1_id`),
  KEY `fk_match_team2` (`int_team2_id`),
  KEY `fk_match_venue` (`int_venue_id`),
  KEY `fk_match_match_season` (`int_match_season_id`),
  KEY `fk_match_tosswinnerteam` (`int_toss_winner_team_id`),
  KEY `fk_match_winnerteam` (`int_winner_team_id`),
  KEY `fk_match_player` (`int_player_of_the_match_id`),
  CONSTRAINT `fk_match_match_season` FOREIGN KEY (`int_match_season_id`) REFERENCES `tbl_match_season` (`int_match_season_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_match_player` FOREIGN KEY (`int_player_of_the_match_id`) REFERENCES `tbl_player` (`int_player_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_match_team1` FOREIGN KEY (`int_team1_id`) REFERENCES `tbl_team` (`int_team_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_match_team2` FOREIGN KEY (`int_team2_id`) REFERENCES `tbl_team` (`int_team_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_match_tosswinnerteam` FOREIGN KEY (`int_toss_winner_team_id`) REFERENCES `tbl_team` (`int_team_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_match_venue` FOREIGN KEY (`int_venue_id`) REFERENCES `tbl_venue` (`int_venue_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_match_winnerteam` FOREIGN KEY (`int_winner_team_id`) REFERENCES `tbl_team` (`int_team_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_match`
--

LOCK TABLES `tbl_match` WRITE;
/*!40000 ALTER TABLE `tbl_match` DISABLE KEYS */;
INSERT INTO `tbl_match` VALUES (1,12,12,'2019-01-01',3,1,12,NULL,NULL,NULL,NULL,NULL,'2019-10-06 18:12:02',1,NULL,NULL),(2,12,14,'2019-01-01',3,3,NULL,NULL,NULL,NULL,NULL,14,'2019-10-07 15:21:57',1,NULL,NULL),(3,12,15,'2019-01-02',3,3,NULL,NULL,NULL,NULL,NULL,12,'2019-10-07 15:22:25',1,NULL,NULL),(4,12,18,'2019-01-03',3,3,NULL,NULL,NULL,NULL,NULL,12,'2019-10-07 15:22:43',1,NULL,NULL),(5,12,16,'2019-01-03',3,3,NULL,NULL,NULL,NULL,NULL,16,'2019-10-07 15:23:26',1,NULL,NULL),(6,12,17,'2019-01-05',3,3,NULL,NULL,NULL,NULL,NULL,12,'2019-10-07 15:23:48',1,NULL,NULL),(7,14,17,'2019-01-06',3,3,NULL,NULL,NULL,NULL,NULL,17,'2019-10-07 15:28:33',1,NULL,NULL);
/*!40000 ALTER TABLE `tbl_match` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_match_season`
--

DROP TABLE IF EXISTS `tbl_match_season`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_match_season` (
  `int_match_season_id` int(11) NOT NULL AUTO_INCREMENT,
  `txt_name` varchar(100) DEFAULT NULL,
  `txt_short_name` varchar(25) DEFAULT NULL,
  `dat_created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `dat_modified` datetime DEFAULT NULL,
  `modfied_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`int_match_season_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_match_season`
--

LOCK TABLES `tbl_match_season` WRITE;
/*!40000 ALTER TABLE `tbl_match_season` DISABLE KEYS */;
INSERT INTO `tbl_match_season` VALUES (1,'Indian Premier League 2019','IPL','2019-10-06 17:51:59',1,NULL,NULL),(3,'ICC World Cup 2019','WC 2019','2019-10-07 15:20:07',1,NULL,NULL);
/*!40000 ALTER TABLE `tbl_match_season` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_player`
--

DROP TABLE IF EXISTS `tbl_player`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_player` (
  `int_player_id` int(11) NOT NULL AUTO_INCREMENT,
  `txt_first_name` varchar(50) DEFAULT NULL,
  `txt_middle_name` varchar(50) DEFAULT NULL,
  `txt_last_name` varchar(50) DEFAULT NULL,
  `txt_name` varchar(150) DEFAULT NULL,
  `txt_short_name` varchar(25) DEFAULT NULL,
  `txt_image_url` varchar(255) DEFAULT NULL,
  `txt_jersey_number` varchar(25) DEFAULT NULL,
  `int_team_id` int(11) NOT NULL,
  `dat_dob` date DEFAULT NULL,
  `int_batting_style_type` int(11) DEFAULT NULL,
  `int_balling_style_type` int(11) DEFAULT NULL,
  `dat_created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `dat_modified` datetime DEFAULT NULL,
  `modfied_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`int_player_id`),
  KEY `fk_player_team` (`int_team_id`),
  CONSTRAINT `fk_player_team` FOREIGN KEY (`int_team_id`) REFERENCES `tbl_team` (`int_team_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_player`
--

LOCK TABLES `tbl_player` WRITE;
/*!40000 ALTER TABLE `tbl_player` DISABLE KEYS */;
INSERT INTO `tbl_player` VALUES (3,'Ajinkya ','','Rahane','Ajinkya  Rahane','','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images//players/64X64/3991.png','',12,NULL,NULL,NULL,'2019-10-07 17:30:04',1,NULL,NULL),(4,'Cheteshwar','','Pujara','Cheteshwar Pujara','','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images//players/64X64/3896.png','',12,NULL,NULL,NULL,'2019-10-07 17:31:00',1,NULL,NULL),(5,'Hanuma','','Vihari','Hanuma Vihari','','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images//players/64X64/59641.png','',12,NULL,NULL,NULL,'2019-10-07 17:31:26',1,NULL,NULL),(6,'Mayank','','Agarwal','Mayank Agarwal','','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images//players/64X64/10115.png','',12,NULL,NULL,NULL,'2019-10-07 17:31:55',1,NULL,NULL),(7,'Rohit ','','Sharma','Rohit  Sharma','','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images//players/64X64/3852.png','',12,NULL,NULL,NULL,'2019-10-07 17:32:37',1,NULL,NULL);
/*!40000 ALTER TABLE `tbl_player` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_player_match`
--

DROP TABLE IF EXISTS `tbl_player_match`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_player_match` (
  `int_player_match_id` int(11) NOT NULL AUTO_INCREMENT,
  `int_match_id` int(11) NOT NULL,
  `int_team_id` int(11) NOT NULL,
  `int_player_id` int(11) NOT NULL,
  `int_role_type` int(11) NOT NULL,
  `dat_created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `dat_modified` datetime DEFAULT NULL,
  `modfied_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`int_player_match_id`),
  KEY `fk_player_match_team` (`int_team_id`),
  KEY `fk_player_match_match` (`int_match_id`),
  KEY `fk_player_match_player` (`int_player_id`),
  CONSTRAINT `fk_player_match_match` FOREIGN KEY (`int_match_id`) REFERENCES `tbl_match` (`int_match_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_player_match_player` FOREIGN KEY (`int_player_id`) REFERENCES `tbl_player` (`int_player_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_player_match_team` FOREIGN KEY (`int_team_id`) REFERENCES `tbl_team` (`int_team_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_player_match`
--

LOCK TABLES `tbl_player_match` WRITE;
/*!40000 ALTER TABLE `tbl_player_match` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_player_match` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_team`
--

DROP TABLE IF EXISTS `tbl_team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_team` (
  `int_team_id` int(11) NOT NULL AUTO_INCREMENT,
  `txt_name` varchar(100) DEFAULT NULL,
  `txt_short_name` varchar(25) DEFAULT NULL,
  `txt_logo_url` varchar(255) DEFAULT NULL,
  `txt_club_state` varchar(100) DEFAULT NULL,
  `int_country_id` int(11) NOT NULL,
  `dat_created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `dat_modified` datetime DEFAULT NULL,
  `modfied_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`int_team_id`),
  KEY `fk_team_country` (`int_country_id`),
  CONSTRAINT `fk_team_country` FOREIGN KEY (`int_country_id`) REFERENCES `tbl_country` (`int_country_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_team`
--

LOCK TABLES `tbl_team` WRITE;
/*!40000 ALTER TABLE `tbl_team` DISABLE KEYS */;
INSERT INTO `tbl_team` VALUES (12,'India','IND','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/flags//90x50/4.png','CS',9,'2019-10-06 16:53:17',1,NULL,NULL),(14,'England','ENG','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/flags//90x50/3.png','',12,'2019-10-07 15:09:56',1,NULL,NULL),(15,'Australia','AUS','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/flags//90x50/1.png','',10,'2019-10-07 15:10:20',1,NULL,NULL),(16,'New Zealand','NZ','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/flags//90x50/5.png','',16,'2019-10-07 15:10:57',1,NULL,NULL),(17,'Pakistan','PAK','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/flags//90x50/6.png','',15,'2019-10-07 15:12:15',1,NULL,NULL),(18,'Sri Lanka','SL','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/flags//90x50/8.png','',8,'2019-10-07 15:12:37',1,NULL,NULL),(19,'South Africa','SA','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/flags//90x50/7.png','',14,'2019-10-07 15:12:54',1,NULL,NULL),(20,'Bangladesh','BAN','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/flags//90x50/2.png','',17,'2019-10-07 15:13:14',1,NULL,NULL),(21,'West Indies','WI','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/flags//90x50/9.png','',13,'2019-10-07 15:13:41',1,NULL,NULL),(22,'Afghanistan','AFG','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/flags//90x50/1188.png','',18,'2019-10-07 15:14:02',1,NULL,NULL);
/*!40000 ALTER TABLE `tbl_team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_venue`
--

DROP TABLE IF EXISTS `tbl_venue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_venue` (
  `int_venue_id` int(11) NOT NULL AUTO_INCREMENT,
  `txt_name` varchar(100) DEFAULT NULL,
  `txt_short_name` varchar(25) DEFAULT NULL,
  `int_country_id` int(11) NOT NULL,
  `dat_created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `dat_modified` datetime DEFAULT NULL,
  `modfied_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`int_venue_id`),
  KEY `fk_venue_country_idx` (`int_country_id`),
  CONSTRAINT `fk_venue_country` FOREIGN KEY (`int_country_id`) REFERENCES `tbl_country` (`int_country_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_venue`
--

LOCK TABLES `tbl_venue` WRITE;
/*!40000 ALTER TABLE `tbl_venue` DISABLE KEYS */;
INSERT INTO `tbl_venue` VALUES (3,'Noida Stadium','Noida Stadium',8,'2019-10-06 18:11:23',1,NULL,NULL);
/*!40000 ALTER TABLE `tbl_venue` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-08 18:58:47

SQL;
$this->execute($strQuery);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191008_132730_all_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191008_132730_all_tables cannot be reverted.\n";

        return false;
    }
    */
}
