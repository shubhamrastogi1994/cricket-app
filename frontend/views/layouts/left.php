<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user1-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Shubham Rastogi</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
<!--        <form action="#" method="get" class="sidebar-form">-->
<!--            <div class="input-group">-->
<!--                <input type="text" name="q" class="form-control" placeholder="Search..."/>-->
<!--              <span class="input-group-btn">-->
<!--                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>-->
<!--                </button>-->
<!--              </span>-->
<!--            </div>-->
<!--        </form>-->
        <!-- /.search form -->

        <?php
        $objCategory = new \app\models\Category();

        echo dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Academics', 'icon' => 'calendar-o','options' => ['class' => 'header']],
                    [
                        'label' => 'Course Management',
                        'icon' => 'graduation-cap',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Academic Year', 'icon' => 'calendar', 'url' => ['/course/academic-year']],
                            ['label' => 'Course', 'icon' => 'graduation-cap', 'url' => ['/course/course']],
                            ['label' => 'Year Wise Course', 'icon' => 'info-circle', 'url' => ['/course/academic-year-course']],
                            ['label' => 'Subject', 'icon' => 'book', 'url' => ['/course/subjects']],
                            ['label' => 'Batch', 'icon' => 'sitemap', 'url' => ['/course/batch']],
                            ['label' => 'Section', 'icon' => 'share-alt', 'url' => ['/course/section']],
                        ],
                    ],
// TODO: Quiz Section Later
//                    [
//                        'label' => 'Online Test',
//                        'icon' => 'list',
//                        'url' => '#',
//                        'items' => [
//                            ['label' => 'Question Category', 'icon' => 'link', 'url' => ['/course/academic-year']],
//                            ['label' => 'Questions', 'icon' => 'question-circle', 'url' => ['/course/course']],
//                            ['label' => 'Grading System', 'icon' => 'sort-alpha-asc', 'url' => ['/course/academic-year-course']],
//                            ['label' => 'Online Test', 'icon' => 'list-alt', 'url' => ['/course/subjects']],
//                            ['label' => 'View Result', 'icon' => 'file-text', 'url' => ['/course/batch']],
//                            ['label' => 'Import Questions', 'icon' => 'upload', 'url' => ['/course/section']],
//                        ],
//                    ],
                    [
                        'label' => 'Time Table',
                        'icon' => 'calendar-o',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Room Category', 'icon' => 'sort-alpha-asc', 'url' => ['/category/index','intCategoryId'=>$objCategory->getRoomCategoryId()]],
                            ['label' => 'Room Master', 'icon' => 'object-ungroup', 'url' => ['/timetable/room-master']],
                            ['label' => 'Class Timing', 'icon' => 'clock-o', 'url' => ['/timetable/class-timing']],
                            ['label' => 'Timetable Type', 'icon' => 'sort-alpha-asc', 'url' => ['/category/index','intCategoryId'=>$objCategory->getTimeTableTypeCategoryId()]],
                            ['label' => 'Attendance Type', 'icon' => 'sort-alpha-asc', 'url' => ['/category/index','intCategoryId'=>$objCategory->getAttendanceTypeCategoryId()]],
                            ['label' => 'Time Table', 'icon' => 'calendar', 'url' => ['/timetable/time-table-configuration']],
                        ],
                    ],
//					[
//                        'label' => 'Examination',
//                        'icon' => 'clipboard',
//                        'url' => '#',
//                        'items' => [
//							['label' => 'Grading System', 'icon' => 'sort-alpha-asc', 'url' => ['/timetable/room-master']],
//                          ['label' => 'Exam Group', 'icon' => 'link', 'url' => ['/timetable/room-category']],
//
//                        ],
//                    ],
//
//
                    [
                        'label' => 'Student Attendance',
                        'icon' => 'check-square-o',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Manage Student Attendance', 'icon' => 'check-square', 'url' => ['/attendance/student-attendance']],
                            ['label' => 'Lecture Attendance', 'icon' => 'check-square', 'url' => ['/attendance/student-attendance/day-wise-attendance']],
                        ],
                    ],
//					[
//                        'label' => 'Academics',
//                        'icon' => 'calendar-o',
//                        'url' => '#',
//                        'items' => [
//                            ['label' => 'Assignment', 'icon' => 'object-group', 'url' => ['/timetable/room-category']],
//                        ],
//                    ],
//                    [
//                        'label' => 'Placement',
//                        'icon' => 'map-marker',
//                        'url' => '#',
//                        'items' => [
//                            ['label' => 'Recruiter', 'icon' => 'user', 'url' => ['/timetable/room-category']],
//                            ['label' => 'Jobs', 'icon' => 'search', 'url' => ['/timetable/room-master']],
//                        ],
//                    ],
                    ['label' => 'Human Resource','icon' => 'user', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Employee Management',
                        'icon' => 'user',
                        'url' => '#',
                        'items' => [
//                            ['label' => 'Employee Management', 'icon' => 'user', 'url' => ['/timetable/room-category']],
                            ['label' => 'Department', 'icon' => 'sitemap', 'url' => ['/category/index','intCategoryId'=>$objCategory->getDepartmentCategoryId()]],
							['label' => 'Designation', 'icon' => 'signal', 'url' => ['/category/index','intCategoryId'=>$objCategory->getDesignationCategoryId()]],
							['label' => 'Add Employee', 'icon' => 'user-plus', 'url' => ['/employee/employee-master/create']],
							['label' => 'Manage Employee', 'icon' => 'reorder', 'url' => ['/employee/employee-master']],
//							['label' => 'Import Employee', 'icon' => 'upload', 'url' => ['/timetable/room-master']],
//							['label' => 'Shift Allocation', 'icon' => 'plus-square', 'url' => ['/timetable/room-master']],
//							['label' => 'Employee Settings', 'icon' => 'gear', 'url' => ['/timetable/room-master']],
                        ],
                    ],
                    [
                        'label' => 'Employee Configuration',
                        'icon' => 'gears',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Shift', 'icon' => 'clock-o', 'url' => ['/hr/shift']],
//                          ['label' => 'Loan Type', 'icon' => 'life-bouy', 'url' => ['/timetable/room-master']],
//							['label' => 'Week off', 'icon' => 'calendar', 'url' => ['/timetable/room-master']],
//							['label' => 'National Holiday', 'icon' => 'calendar-o', 'url' => ['/timetable/room-master']],
                        ],
                    ],
//                    [
//                        'label' => 'Leave Management',
//                        'icon' => 'file-text-o',
//                        'url' => '#',
//                        'items' => [
//                            ['label' => 'Leave Management', 'icon' => 'file-text-o', 'url' => ['/timetable/room-category']],
//                            ['label' => 'Leave Type', 'icon' => 'life-bouy', 'url' => ['/category/index','intCategoryId'=>$intDesignationCategoryId]],
//							['label' => 'Leave Structure', 'icon' => 'clock-o', 'url' => ['/timetable/room-master']],
//							['label' => 'Leave Entitlement', 'icon' => 'plus-square', 'url' => ['/timetable/room-master']],
//							['label' => 'Leave Reporting', 'icon' => 'users', 'url' => ['/timetable/room-master']],
//							['label' => 'Leave Applications', 'icon' => 'list-alt', 'url' => ['/timetable/room-category']],
//                        ],
//                    ],
//                    [
//                        'label' => 'Attendance',
//                        'icon' => 'check-square-o',
//                        'url' => '#',
//                        'items' => [
//                            ['label' => 'Take Attendance', 'icon' => 'check-square-o', 'url' => ['/timetable/room-category']],
//                            ['label' => 'Manage Attendance', 'icon' => 'check-square-o', 'url' => ['/timetable/room-master']],
//                        ],
//                    ],
//                    [
//                        'label' => 'Payroll',
//                        'icon' => 'bullseye',
//                        'url' => '#',
//                        'items' => [
//                            //['label' => 'Payroll', 'icon' => 'bullseye', 'url' => ['/timetable/room-category']],
//                            ['label' => 'Salary Component', 'icon' => 'tasks', 'url' => ['/timetable/room-master']],
//							['label' => 'Salary Structure', 'icon' => 'clock-o', 'url' => ['/timetable/room-master']],
//							['label' => 'Employee Salary Component', 'icon' => 'check-square-o', 'url' => ['/timetable/room-master']],
//							['label' => 'Employee Loan', 'icon' => 'money', 'url' => ['/timetable/room-master']],
//							['label' => 'Salary Slip', 'icon' => 'credit-card', 'url' => ['/timetable/room-master']],
//							['label' => 'Print Salary Slip', 'icon' => 'print', 'url' => ['/timetable/room-master']],
//							['label' => 'Publish Salary Slip', 'icon' => 'bullhorn', 'url' => ['/timetable/room-master']],
//							
//                        ],
//                    ],
                    ['label' => 'Student','icon' => 'users', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Student',
                        'icon' => 'users',
                        'url' => '#',
                        'items' => [
//                            ['label' => 'Student', 'icon' => 'users', 'url' => ['/timetable/room-category']],
                            ['label' => 'Admission Category', 'icon' => 'sort-alpha-asc', 'url' => ['/category/index','intCategoryId'=>$objCategory->getStudentCategoryId()]],
							['label' => 'Add Student', 'icon' => 'user-plus', 'url' => ['/student/student-master/create']],
							['label' => 'Manage Student', 'icon' => 'reorder', 'url' => ['/student/student-master']],
//							['label' => 'Import Student', 'icon' => 'upload', 'url' => ['/student/student-master/import']],
							['label' => 'Student Status', 'icon' => 'info-circle', 'url' => ['/category/index','intCategoryId'=>$objCategory->getStudentStatusCategoryId()]],
//							['label' => 'Promote Student', 'icon' => 'exchange', 'url' => ['/timetable/room-master']],
                        ],
                    ],
//                    [
//                        'label' => 'Enquiry ',
//                        'icon' => 'user-plus',
//                        'url' => '#',
//                        'items' => [
//                            ['label' => 'Online Application', 'icon' => 'external-link', 'url' => ['/timetable/room-category']],
//                            ['label' => 'Manage Enquiry', 'icon' => 'users', 'url' => ['/timetable/room-master']],
//							['label' => 'Admission Letter', 'icon' => 'file-text-o', 'url' => ['/timetable/room-master']],
//                        ],
//                    ],
//                    ['label' => 'Fees', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Fees',
                        'icon' => 'money',
                        'url' => '#',
                        'items' => [
//                            ['label' => 'Fees', 'icon' => 'money', 'url' => ['/timetable/room-category']],
                            ['label' => 'Bank Master', 'icon' => 'bank', 'url' => ['/category/index','intCategoryId'=>$objCategory->getBankMasterCategoryId()]],
//							['label' => 'Fees Category', 'icon' => 'sort-alpha-asc', 'url' => ['/timetable/room-master']],
//							['label' => 'Other Fees Category', 'icon' => 'sort-alpha-asc', 'url' => ['/timetable/room-master']],
//							['label' => 'Assign Fees', 'icon' => 'exchange', 'url' => ['/timetable/room-master']],
//							['label' => 'Student Fees', 'icon' => 'money', 'url' => ['/timetable/room-master']],
                        ],
                    ],
//                    ['label' => 'Communication','icon' => 'comments', 'options' => ['class' => 'header']],
//                    [
//                        'label' => 'DashBoard Management',
//                        'icon' => 'dashboard',
//                        'url' => '#',
//                        'items' => [
//                            ['label' => 'DashBoard Management', 'icon' => 'dashboard', 'url' => ['/timetable/room-category']],
//                            ['label' => 'Message of the Day', 'icon' => 'list-alt', 'url' => ['/timetable/room-master']],
//							['label' => 'Notice', 'icon' => 'columns', 'url' => ['/timetable/room-master']],
//							['label' => 'Events', 'icon' => 'flag', 'url' => ['/timetable/room-master']],
//							['label' => 'Task', 'icon' => 'check-circle', 'url' => ['/timetable/room-master']],
//							['label' => 'Calendar', 'icon' => 'calendar', 'url' => ['/timetable/room-master']],
//                        ],
//                    ],
//                    [
//                        'label' => 'SMS',
//                        'icon' => 'comment',
//                        'url' => '#',
//                        'items' => [
//                            ['label' => 'Employee SMS', 'icon' => 'comments-o', 'url' => ['/timetable/room-category']],
//                            ['label' => 'Student SMS', 'icon' => 'comments-o', 'url' => ['/timetable/room-master']],
//                        ],
//                    ],
//					[
//                        'label' => 'Email',
//                        'icon' => 'envelope-o',
//                        'url' => '#',
//                        'items' => [
//                            ['label' => 'Employee Email', 'icon' => 'comments-o', 'url' => ['/timetable/room-category']],
//                            ['label' => 'Student Email', 'icon' => 'comments-o', 'url' => ['/timetable/room-master']],
//                        ],
//                    ],
//					[
//                        'label' => 'Telephone Dairy',
//                        'icon' => 'phone',
//                        'url' => '#',
//                        'items' => [
//                            ['label' => 'Student Contact', 'icon' => 'phone-square', 'url' => ['/timetable/room-category']],
//                            ['label' => 'Employee Contact', 'icon' => 'phone-square', 'url' => ['/timetable/room-master']],
//                        ],
//                    ],
//                    [
//                        'label' => 'Helpdesk',
//                        'icon' => 'question-circle',
//                        'url' => '#',
//                        'items' => [
//                            ['label' => 'Inquiry Subjects', 'icon' => 'question', 'url' => ['/timetable/room-category']],
//                            ['label' => 'Inquiry Tickets', 'icon' => 'ticket', 'url' => ['/timetable/room-master']],
//                        ],
//                    ],
//
//                    [
//                        'label' => 'Parent Communication',
//                        'icon' => 'commenting',
//                        'url' => '#',
//                        'items' => [
//                            ['label' => 'Settings', 'icon' => 'gear', 'url' => ['/timetable/room-category']],
//                        ],
//                    ],
//
//                    ['label' => 'Reports Center','icon' => 'line-chart','options' => ['class' => 'header']],
//                    [
//                        'label' => 'Reports Center',
//                        'icon' => 'bar-chart-o',
//                        'url' => '#',
//                        'items' => [
//                            //['label' => 'Recruiter', 'icon' => 'dashboard', 'url' => ['/timetable/room-category']],
//                            ['label' => 'Employee', 'icon' => 'bar-chart', 'url' => ['/timetable/room-master']],
//							['label' => 'Student', 'icon' => 'bar-chart', 'url' => ['/timetable/room-master']],
//							['label' => 'Fees', 'icon' => 'bar-chart', 'url' => ['/timetable/room-master']],
//							['label' => 'Daily Fees Collection', 'icon' => 'bar-chart', 'url' => ['/timetable/room-master']],
//							['label' => 'Leave', 'icon' => 'bar-chart', 'url' => ['/timetable/room-master']],
//							['label' => 'Transport', 'icon' => 'bar-chart', 'url' => ['/timetable/room-master']],
//							['label' => 'Assignment', 'icon' => 'bar-chart', 'url' => ['/timetable/room-master']],
//                        ],
//                    ],
//                    [
//                        'label' => 'Hostel Report',
//                        'icon' => 'building-o',
//                        'url' => '#',
//                        'items' => [
//                            ['label' => 'Fees Report', 'icon' => 'money', 'url' => ['/timetable/room-category']],
//                            ['label' => 'Occupied Room', 'icon' => 'info-circle', 'url' => ['/timetable/room-master']],
//							['label' => 'Unoccupied Room', 'icon' => 'info-circle', 'url' => ['/timetable/room-master']],
//                        ],
//                    ],
//					[
//                        'label' => 'Enquiry',
//                        'icon' => 'check-square-o',
//                        'url' => '#',
//                        'items' => [
//                            //['label' => 'Enquiry', 'icon' => 'dashboard', 'url' => ['/timetable/room-category']],
//                            ['label' => 'Enquiry Report', 'icon' => 'users', 'url' => ['/timetable/room-master']],
//							['label' => 'Follow-up Report', 'icon' => 'random', 'url' => ['/timetable/room-master']],
//                        ],
//                    ],
//					 [
//                        'label' => 'Attendance',
//                        'icon' => 'check-square-o',
//                        'url' => '#',
//                        'items' => [
//                            ['label' => 'Employee', 'icon' => 'check-square', 'url' => ['/timetable/room-category']],
//                            ['label' => 'Student', 'icon' => 'check-square', 'url' => ['/timetable/room-master']],
//                        ],
//                    ],
//                    [
//                        'label' => 'Time Table',
//                        'icon' => 'calendar-o',
//                        'url' => '#',
//                        'items' => [
//                            ['label' => 'Daily Schedule', 'icon' => 'calendar', 'url' => ['/timetable/room-category']],
//                        ],
//                    ],
//                    [
//                        'label' => 'Exam Report',
//                        'icon' => 'clipboard',
//                        'url' => '#',
//                        'items' => [
//                            //['label' => 'Exam Report', 'icon' => 'area-chart', 'url' => ['/timetable/room-category']],
//                            ['label' => 'Exam Group Report', 'icon' => 'area-chart', 'url' => ['/timetable/room-master']],
//							['label' => 'Subject Wise', 'icon' => 'area-chart', 'url' => ['/timetable/room-category']],
//							['label' => 'Grouped Exam Wise', 'icon' => 'area-chart', 'url' => ['/timetable/room-category']],
//                        ],
//                    ],
//                    ['label' => 'Administration','icon' => 'wrench', 'options' => ['class' => 'header']],
//                    [
//                        'label' => 'Hostel',
//                        'icon' => 'building',
//                        'url' => '#',
//                        'items' => [
//                            ['label' => 'Hostel Type', 'icon' => 'sort-alpha-asc', 'url' => ['/timetable/room-category']],
//                            ['label' => 'Hostel Details', 'icon' => 'building-o', 'url' => ['/timetable/room-master']],
//							['label' => 'Hostel Blocks', 'icon' => 'sitemap', 'url' => ['/timetable/room-category']],
//							['label' => 'Room Details', 'icon' => 'building', 'url' => ['/timetable/room-category']],
//							['label' => 'Fees Structure', 'icon' => 'money', 'url' => ['/timetable/room-category']],
//							['label' => 'Student Registration', 'icon' => 'user-plus', 'url' => ['/timetable/room-category']],
//							['label' => 'Registered Students', 'icon' => 'users', 'url' => ['/timetable/room-category']],
//                        ],
//                    ],
//                    [
//                        'label' => 'Transport',
//                        'icon' => 'bus',
//                        'url' => '#',
//                        'items' => [
//							['label' => 'Vehicle Details', 'icon' => 'ambulance', 'url' => ['/timetable/room-category']],
//							['label' => 'Driver Details', 'icon' => 'user-plus', 'url' => ['/timetable/room-category']],
//						    ['label' => 'Manage Route', 'icon' => 'calendar', 'url' => ['/timetable/room-category']],
//                            ['label' => 'Student Bus Allocation', 'icon' => 'list-alt', 'url' => ['/timetable/room-category']],
//                            ['label' => 'Fees Collect', 'icon' => 'money', 'url' => ['/timetable/room-master']],
//                        ],
//                    ],
//                    [
//                        'label' => 'Account',
//                        'icon' => 'book',
//                        'url' => '#',
//                        'items' => [
//                            ['label' => 'Exp. Category', 'icon' => 'file-text', 'url' => ['/timetable/room-category']],
//                            ['label' => 'Payable', 'icon' => 'file-text', 'url' => ['/timetable/room-master']],
//							['label' => 'Expenses', 'icon' => 'file', 'url' => ['/timetable/room-master']],
//                        ],
//                    ],
//                    ['label' => 'Document','icon' => 'file-text-o', 'options' => ['class' => 'header']],
//                    [
//                        'label' => 'Certificate/Letters',
//                        'icon' => 'certificate',
//                        'url' => '#',
//                        'items' => [
//                            ['label' => 'Manage Certificate/Letter', 'icon' => 'file-text', 'url' => ['/timetable/room-category']],
//                            ['label' => 'Student Certificate/Letter', 'icon' => 'server', 'url' => ['/timetable/room-master']],
//							['label' => 'Employee Certificate/Letter', 'icon' => 'server', 'url' => ['/timetable/room-master']],
//
//                        ],
//                    ],
                    [
                        'label' => 'Manage Documents',
                        'icon' => 'files-o',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Document Category', 'icon' => 'reorder', 'url' => ['/category/index','intCategoryId'=>$objCategory->getDocumentCategoryId()]],
//                            ['label' => 'Student Documents', 'icon' => 'file-o', 'url' => ['#']],
//							['label' => 'Employee Documents', 'icon' => 'file-o', 'url' => ['#']],
                        ],
                    ],
//                    [
//                        'label' => 'File Sharing',
//                        'icon' => 'share-alt',
//                        'url' => '#',
//                        'items' => [
//                            ['label' => 'File Category', 'icon' => 'list', 'url' => ['/timetable/room-category']],
//                            ['label' => 'File Uploads', 'icon' => 'file-text', 'url' => ['/timetable/room-master']],
//                        ],
//                    ],
//                    ['label' => 'Library', 'icon' => 'university','options' => ['class' => 'header']],
//                    [
//                        'label' => 'Library',
//                        'icon' => 'university',
//                        'url' => '#',
//                        'items' => [
//                            ['label' => 'Book Category', 'icon' => 'sort-alpha-asc', 'url' => ['/timetable/room-category']],
//                            ['label' => 'Cup Board', 'icon' => 'book', 'url' => ['/timetable/room-master']],
//							['label' => 'Cup Board Shelf', 'icon' => 'columns', 'url' => ['/timetable/room-master']],
//							['label' => 'Book Vendor', 'icon' => 'user-o', 'url' => ['/timetable/room-master']],
//							['label' => 'Book Status', 'icon' => 'tag', 'url' => ['/timetable/room-master']],
//							['label' => 'Books', 'icon' => 'book', 'url' => ['/timetable/room-master']],
//							['label' => 'Issue Book', 'icon' => 'book', 'url' => ['/timetable/room-master']],
//							['label' => 'Return/Renew Book', 'icon' => 'reply-all', 'url' => ['/timetable/room-master']],
//							['label' => 'Fine', 'icon' => 'eject', 'url' => ['/timetable/room-master']],
//                        ],
//                    ],

                    ['label' => 'Setting','icon' => 'cogs', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Configuration',
                        'icon' => 'cogs',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Country', 'icon' => 'globe', 'url' => ['/country']],
                            ['label' => 'State/Province', 'icon' => 'map-marker', 'url' => ['/state']],
							['label' => 'City/Town', 'icon' => 'building-o', 'url' => ['/city']],
							['label' => 'Languages', 'icon' => 'language', 'url' => ['/category/index','intCategoryId'=>$objCategory->getLanguageCategoryId()]],
							['label' => 'Nationality', 'icon' => 'flag-checkered', 'url' => ['/category/index','intCategoryId'=> $objCategory->getNationalityCategoryId()]],
							['label' => 'Institute', 'icon' => 'bank', 'url' => ['/organization/view'],'visible'=> !empty(Yii::$app->user->identity->int_org_id)],
                            ['label' => 'Institutes', 'icon' => 'bank', 'url' => ['/organization/'],'visible'=> empty(Yii::$app->user->identity->int_org_id)],
                        ],
                    ],
//                    [
//                        'label' => 'Manage Users',
//                        'icon' => 'users',
//                        'url' => '#',
//                        'items' => [
//                            ['label' => 'Student Reset Password', 'icon' => 'key', 'url' => ['/timetable/room-category']],
//                            ['label' => 'Employee Reset Password', 'icon' => 'key', 'url' => ['/timetable/room-master']],
//							['label' => 'Manage Parent', 'icon' => 'key', 'url' => ['/timetable/room-master']],
//                        ],
//                    ],
//                    [
//                        'label' => 'Manage User Rights',
//                        'icon' => 'user-secret',
//                        'url' => '#',
//                        'items' => [
//                            ['label' => 'Assignment', 'icon' => 'male', 'url' => ['/timetable/room-category']],
//                            ['label' => 'Role', 'icon' => 'user-times', 'url' => ['/timetable/room-master']],
//                        ],
//                    ],
//                    [
//                        'label' => 'Additional',
//                        'icon' => 'cog',
//                        'url' => '#',
//                        'items' => [
//                            ['label' => 'System Settings', 'icon' => 'cogs', 'url' => ['/timetable/room-category']],
//                            ['label' => 'Notification Settings', 'icon' => 'bell-o', 'url' => ['/timetable/room-master']],
//                            ['label' => 'Backup', 'icon' => 'database', 'url' => ['/timetable/room-category']],
//                            ['label' => 'Login History', 'icon' => 'history', 'url' => ['/timetable/room-category']],
//                        ],
//                    ],





//                    ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    ['label' => 'Logout', 'url' => ['site/logout'], 'visible' => !Yii::$app->user->isGuest],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
//                    [
//                        'label' => 'Some tools',
//                        'icon' => 'share',
//                        'url' => '#',
//                        'items' => [
//                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
//                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
//                            [
//                                'label' => 'Level One',
//                                'icon' => 'circle-o',
//                                'url' => '#',
//                                'items' => [
//                                    ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
//                                    [
//                                        'label' => 'Level Two',
//                                        'icon' => 'circle-o',
//                                        'url' => '#',
//                                        'items' => [
//                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
//                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
//                                        ],
//                                    ],
//                                ],
//                            ],
//                        ],
//                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
