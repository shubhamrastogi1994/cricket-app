<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */


if (Yii::$app->controller->action->id === 'login') { 
/**
 * Do not use this code in your template. Remove it. 
 * Instead, use the code  $this->layout = '//main-login'; in your controller.
 */
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
}elseif (Yii::$app->controller->action->id === 'signup'){
    echo $this->render(
        'main-register',
        ['content' => $content]
    );
}
else {

    if (class_exists('backend\assets\AppAsset')) {
        backend\assets\AppAsset::register($this);
    } else {
        app\assets\AppAsset::register($this);
    }
    \frontend\assets\AppAsset::register($this);

    dmstr\web\AdminLteAsset::register($this);

    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= (!empty($this->title)?Html::encode($this->title): $this->title = 'StudiersHub')?></title>
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <style>
            .navbar {
                display: block;
                padding: 0;
            }
            .navbar-nav {
                display: block !important;
            }
        </style>
        <?php $this->head() ?>
    </head>
    <?php
    if (Yii::$app->controller->id === 'organization' && Yii::$app->controller->action->id === 'create'){
        $sidebarClass = 'sidebar-collapse';
    }else{
        $sidebarClass = 'sidebar-mini sidebar-collapse';
    }





    ?>
    <body class="hold-transition skin-blue <?= $sidebarClass  ?> ">
    <?php $this->beginBody() ?>

    <div class="wrapper">
        <?php
        if (Yii::$app->controller->id === 'organization' && Yii::$app->controller->action->id === 'create'){
            echo  $this->render(
                'content.php',
                ['content' => $content, 'directoryAsset' => $directoryAsset]
            );
        }else{
            echo $this->render(
                'header.php',
                ['directoryAsset' => $directoryAsset]
            );

        echo $this->render(
            'left.php',
            ['directoryAsset' => $directoryAsset]
        );
        echo  $this->render(
            'content.php',
            ['content' => $content, 'directoryAsset' => $directoryAsset]
        );
        }
         ?>



    </div>

    <?php $this->endBody() ?>
    <?php
    \yii\bootstrap\Modal::begin([
//        'headerOptions' => ['id' => 'modalHeader'],
        'id' => 'globalModal',
        'size' => 'modal-lg modal-md modal-sm',

        //keeps from closing modal with esc key or by clicking out of the modal.
        // user must click cancel or X to close
        'clientOptions' => [

//                'backdrop' => 'static', 'keyboard' => FALSE
        ],
        'closeButton' => false,
        'options' => [
          'aria-labelledby'=> 'modalLabel',
          'aria-hidden'=> 'true',
        'tabindex' => false,
        ],
    ]);
    echo "<div class=\"loader text-center\"><div class=\"spinner\"><i class=\"fa fa-spinner fa-pulse fa-5x fa-fw\"></i></div></div>";
    yii\bootstrap\Modal::end();
    ?>

    </body>

    </html>
    <?php $this->endPage() ?>
<?php } ?>
