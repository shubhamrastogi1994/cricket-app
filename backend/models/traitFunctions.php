<?php

namespace backend\models;


use Yii;
use yii\web\Response;
use yii\widgets\ActiveForm;

trait traitFunctions
{
    function saveData($model)
    {
        $arrPost = Yii::$app->request->post();
        if ($model->load($arrPost) && Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return array_merge(ActiveForm::validate($model));
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Record saved successfully.');
            return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }
}