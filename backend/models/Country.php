<?php

namespace backend\models;

use common\models\BaseModel;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_country".
 *
 * @property int $int_country_id
 * @property string $txt_name
 * @property string $txt_short_name
 * @property string $txt_code
 * @property string $dat_created
 * @property int $created_by
 * @property string $dat_modified
 * @property int $modfied_by
 *
 * @property TblTeam[] $tblTeams
 * @property TblVenue[] $tblVenues
 */
class Country extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_country';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(),[
            [['txt_name','txt_short_name'], 'required'],
            [['dat_created', 'dat_modified'], 'safe'],
            [['created_by', 'modfied_by'], 'integer'],
            [['txt_name'], 'string', 'max' => 100],
            [['txt_short_name'], 'string', 'max' => 25],
            [['txt_code'], 'string', 'max' => 10],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'int_country_id' => Yii::t('app', 'Country ID'),
            'txt_name' => Yii::t('app', 'Name'),
            'txt_short_name' => Yii::t('app', 'Short Name'),
            'txt_code' => Yii::t('app', 'Code'),
            'dat_created' => Yii::t('app', 'Dat Created'),
            'created_by' => Yii::t('app', 'Created By'),
            'dat_modified' => Yii::t('app', 'Dat Modified'),
            'modfied_by' => Yii::t('app', 'Modfied By'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getTeams()
    {
        return $this->hasMany(Team::className(), ['int_country_id' => 'int_country_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getVenues()
    {
        return $this->hasMany(Venue::className(), ['int_country_id' => 'int_country_id']);
    }
}
