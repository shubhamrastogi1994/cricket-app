<?php

namespace backend\models;

use common\models\BaseModel;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_venue".
 *
 * @property int $int_venue_id
 * @property string $txt_name
 * @property string $txt_short_name
 * @property int $int_country_id
 * @property string $dat_created
 * @property int $created_by
 * @property string $dat_modified
 * @property int $modfied_by
 *
 * @property Match[] $tblMatches
 * @property Country $intCountry
 */
class Venue extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_venue';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(),[
            [['int_country_id'], 'required'],
            [['int_country_id', 'created_by', 'modfied_by'], 'integer'],
            [['dat_created', 'dat_modified'], 'safe'],
            [['txt_name'], 'string', 'max' => 100],
            [['txt_short_name'], 'string', 'max' => 25],
            [['int_country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['int_country_id' => 'int_country_id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'int_venue_id' => Yii::t('app', 'Venue ID'),
            'txt_name' => Yii::t('app', 'Name'),
            'txt_short_name' => Yii::t('app', 'Short Name'),
            'int_country_id' => Yii::t('app', 'Country ID'),
            'dat_created' => Yii::t('app', 'Dat Created'),
            'created_by' => Yii::t('app', 'Created By'),
            'dat_modified' => Yii::t('app', 'Dat Modified'),
            'modfied_by' => Yii::t('app', 'Modfied By'),
            'country.txt_name' => Yii::t('app', 'Country'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getMatches()
    {
        return $this->hasMany(Match::className(), ['int_venue_id' => 'int_venue_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['int_country_id' => 'int_country_id']);
    }
}
