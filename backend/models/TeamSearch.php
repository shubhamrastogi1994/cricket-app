<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * TeamSearch represents the model behind the search form of `backend\models\Team`.
 */
class TeamSearch extends Team
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['int_team_id', 'int_country_id', 'created_by', 'modfied_by'], 'integer'],
            [['txt_name', 'txt_short_name', 'txt_logo_url', 'txt_club_state', 'dat_created', 'dat_modified'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Team::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'int_team_id' => $this->int_team_id,
            'int_country_id' => $this->int_country_id,
            'dat_created' => $this->dat_created,
            'created_by' => $this->created_by,
            'dat_modified' => $this->dat_modified,
            'modfied_by' => $this->modfied_by,
        ]);

        $query->andFilterWhere(['like', 'txt_name', $this->txt_name])
            ->andFilterWhere(['like', 'txt_short_name', $this->txt_short_name])
            ->andFilterWhere(['like', 'txt_logo_url', $this->txt_logo_url])
            ->andFilterWhere(['like', 'txt_club_state', $this->txt_club_state]);

        return $dataProvider;
    }
}
