<?php

namespace backend\models;

use common\models\BaseModel;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_match_season".
 *
 * @property int $int_match_season_id
 * @property string $txt_name
 * @property string $txt_short_name
 * @property string $dat_created
 * @property int $created_by
 * @property string $dat_modified
 * @property int $modfied_by
 *
 * @property Match[] $tblMatches
 */
class MatchSeason extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_match_season';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(),[
            [['txt_name','txt_short_name'], 'required'],
            [['dat_created', 'dat_modified'], 'safe'],
            [['created_by', 'modfied_by'], 'integer'],
            [['txt_name'], 'string', 'max' => 100],
            [['txt_short_name'], 'string', 'max' => 25],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'int_match_season_id' => Yii::t('app', 'Match Season ID'),
            'txt_name' => Yii::t('app', 'Name'),
            'txt_short_name' => Yii::t('app', 'Short Name'),
            'dat_created' => Yii::t('app', 'Dat Created'),
            'created_by' => Yii::t('app', 'Created By'),
            'dat_modified' => Yii::t('app', 'Dat Modified'),
            'modfied_by' => Yii::t('app', 'Modfied By'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getMatches()
    {
        return $this->hasMany(Match::className(), ['int_match_season_id' => 'int_match_season_id']);
    }
}
