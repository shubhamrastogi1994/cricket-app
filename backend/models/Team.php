<?php

namespace backend\models;

use common\models\BaseModel;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_team".
 *
 * @property int $int_team_id
 * @property string $txt_name
 * @property string $txt_short_name
 * @property string $txt_logo_url
 * @property string $txt_club_state
 * @property int $int_country_id
 * @property string $dat_created
 * @property int $created_by
 * @property string $dat_modified
 * @property int $modfied_by
 *
 * @property Match[] $tblMatches
 * @property Match[] $tblMatches0
 * @property Match[] $tblMatches1
 * @property Match[] $tblMatches2
 * @property Player[] $tblPlayers
 * @property PlayerMatch[] $tblPlayerMatches
 * @property Country $intCountry
 */
class Team extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_team';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(),[
            [['txt_name', 'txt_logo_url','int_country_id'], 'required'],
            [['int_country_id', 'created_by', 'modfied_by'], 'integer'],
            [['dat_created', 'dat_modified'], 'safe'],
            [['txt_name', 'txt_club_state'], 'string', 'max' => 100],
            [['txt_short_name'], 'string', 'max' => 25],
            [['txt_logo_url'], 'url'],
            [['int_country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['int_country_id' => 'int_country_id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'int_team_id' => Yii::t('app', 'Team ID'),
            'txt_name' => Yii::t('app', 'Team Name'),
            'txt_short_name' => Yii::t('app', 'Team Short Name'),
            'txt_logo_url' => Yii::t('app', 'Logo'),
            'txt_club_state' => Yii::t('app', 'Club State'),
            'int_country_id' => Yii::t('app', 'Country'),
            'dat_created' => Yii::t('app', 'Dat Created'),
            'created_by' => Yii::t('app', 'Created By'),
            'dat_modified' => Yii::t('app', 'Dat Modified'),
            'modfied_by' => Yii::t('app', 'Modfied By'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getMatches()
    {
        return $this->hasMany(Match::className(), ['int_team1_id' => 'int_team_id'])->alias('team1');
    }

    /**
     * @return ActiveQuery
     */
    public function getMatches0()
    {
        return $this->hasMany(Match::className(), ['int_team2_id' => 'int_team_id'])->alias('team2');
    }

    /**
     * @return ActiveQuery
     */
    public function getMatches1()
    {
        return $this->hasMany(Match::className(), ['int_toss_winner_team_id' => 'int_team_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getMatches2()
    {
        return $this->hasMany(Match::className(), ['int_winner_team_id' => 'int_team_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPlayers()
    {
        return $this->hasMany(Player::className(), ['int_team_id' => 'int_team_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPlayerMatches()
    {
        return $this->hasMany(PlayerMatch::className(), ['int_team_id' => 'int_team_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getIntCountry()
    {
        return $this->hasOne(Country::className(), ['int_country_id' => 'int_country_id']);
    }
}
