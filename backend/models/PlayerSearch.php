<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PlayerSearch represents the model behind the search form of `backend\models\Player`.
 */
class PlayerSearch extends Player
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['int_player_id', 'int_team_id', 'int_batting_style_type', 'int_balling_style_type', 'created_by', 'modfied_by'], 'integer'],
            [['txt_first_name', 'txt_middle_name', 'txt_last_name', 'txt_name', 'txt_short_name', 'txt_image_url', 'txt_jersey_number', 'dat_dob', 'dat_created', 'dat_modified'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Player::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'int_player_id' => $this->int_player_id,
            'int_team_id' => $this->int_team_id,
            'dat_dob' => $this->dat_dob,
            'int_batting_style_type' => $this->int_batting_style_type,
            'int_balling_style_type' => $this->int_balling_style_type,
            'dat_created' => $this->dat_created,
            'created_by' => $this->created_by,
            'dat_modified' => $this->dat_modified,
            'modfied_by' => $this->modfied_by,
        ]);

        $query->andFilterWhere(['like', 'txt_first_name', $this->txt_first_name])
            ->andFilterWhere(['like', 'txt_middle_name', $this->txt_middle_name])
            ->andFilterWhere(['like', 'txt_last_name', $this->txt_last_name])
            ->andFilterWhere(['like', 'txt_name', $this->txt_name])
            ->andFilterWhere(['like', 'txt_short_name', $this->txt_short_name])
            ->andFilterWhere(['like', 'txt_image_url', $this->txt_image_url])
            ->andFilterWhere(['like', 'txt_jersey_number', $this->txt_jersey_number]);

        return $dataProvider;
    }
}
