<?php

namespace backend\models;

use common\models\BaseModel;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_category".
 *
 * @property int $int_category_id
 * @property int $int_category_type_id
 * @property int $int_type_id
 * @property string $txt_name
 * @property string $dat_created
 * @property int $created_by
 * @property string $dat_modified
 * @property int $modfied_by
 */
class Category extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(),[
            [['int_category_type_id'], 'required'],
            [['int_category_id', 'int_category_type_id', 'int_type_id', 'created_by', 'modfied_by'], 'integer'],
            [['dat_created', 'dat_modified'], 'safe'],
            [['txt_name'], 'string', 'max' => 100],
            [['int_category_id'], 'unique'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'int_category_id' => 'Category ID',
            'int_category_type_id' => 'Category Name/Id',
            'int_type_id' => 'Type Id',
            'txt_name' => 'Name',
            'dat_created' => 'Dat Created',
            'created_by' => 'Created By',
            'dat_modified' => 'Dat Modified',
            'modfied_by' => 'Modfied By',
        ];
    }

    public function getCategoryType()
    {
        if (!empty($this->int_category_type_id)) {
            return self::findOne(['int_category_type_id' => $this->int_category_type_id, 'int_type_id' => null])['txt_name'];
        } else {
            return null;
        }

    }
}
