<?php

namespace backend\models;

use common\models\BaseModel;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_match".
 *
 * @property int $int_match_id
 * @property int $int_team1_id
 * @property int $int_team2_id
 * @property string $dat_match
 * @property int $int_venue_id
 * @property int $int_match_season_id
 * @property int $int_toss_winner_team_id
 * @property int $int_toss_decision_type
 * @property int $int_win_type
 * @property int $int_win_margin
 * @property int $int_player_of_the_match_id
 * @property int $int_winner_team_id
 * @property string $dat_created
 * @property int $created_by
 * @property string $dat_modified
 * @property int $modfied_by
 *
 * @property MatchSeason $intMatchSeason
 * @property Player $intPlayerOfTheMatch
 * @property Team $intTeam1
 * @property Team $intTeam2
 * @property Team $intTossWinnerTeam
 * @property Venue $intVenue
 * @property Team $intWinnerTeam
 * @property PlayerMatch[] $tblPlayerMatches
 */
class Match extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_match';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(),[
            [['int_team1_id', 'int_team2_id', 'int_venue_id', 'int_match_season_id'], 'required'],
            [['int_team1_id', 'int_team2_id', 'int_venue_id', 'int_match_season_id', 'int_toss_winner_team_id', 'int_toss_decision_type', 'int_win_type', 'int_win_margin', 'int_player_of_the_match_id', 'int_winner_team_id', 'created_by', 'modfied_by'], 'integer'],
            [['dat_match', 'dat_created', 'dat_modified'], 'safe'],
            [['int_match_season_id'], 'exist', 'skipOnError' => true, 'targetClass' => MatchSeason::className(), 'targetAttribute' => ['int_match_season_id' => 'int_match_season_id']],
            [['int_player_of_the_match_id'], 'exist', 'skipOnError' => true, 'targetClass' => Player::className(), 'targetAttribute' => ['int_player_of_the_match_id' => 'int_player_id']],
            [['int_team1_id'], 'exist', 'skipOnError' => true, 'targetClass' => Team::className(), 'targetAttribute' => ['int_team1_id' => 'int_team_id']],
            [['int_team2_id'], 'exist', 'skipOnError' => true, 'targetClass' => Team::className(), 'targetAttribute' => ['int_team2_id' => 'int_team_id']],
            [['int_toss_winner_team_id'], 'exist', 'skipOnError' => true, 'targetClass' => Team::className(), 'targetAttribute' => ['int_toss_winner_team_id' => 'int_team_id']],
            [['int_venue_id'], 'exist', 'skipOnError' => true, 'targetClass' => Venue::className(), 'targetAttribute' => ['int_venue_id' => 'int_venue_id']],
            [['int_winner_team_id'], 'exist', 'skipOnError' => true, 'targetClass' => Team::className(), 'targetAttribute' => ['int_winner_team_id' => 'int_team_id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'int_match_id' => Yii::t('app', 'Match'),
            'int_team1_id' => Yii::t('app', 'Team1'),
            'int_team2_id' => Yii::t('app', 'Team2'),
            'dat_match' => Yii::t('app', 'Match Date'),
            'int_venue_id' => Yii::t('app', 'Venue'),
            'int_match_season_id' => Yii::t('app', 'Match Season'),
            'int_toss_winner_team_id' => Yii::t('app', 'Toss Winner Team'),
            'int_toss_decision_type' => Yii::t('app', 'Toss Decision Type'),
            'int_win_type' => Yii::t('app', 'Win Type'),
            'int_win_margin' => Yii::t('app', 'Win Margin'),
            'int_player_of_the_match_id' => Yii::t('app', 'Player Of The Match'),
            'int_winner_team_id' => Yii::t('app', 'Winner Team'),
            'dat_created' => Yii::t('app', 'Dat  Created'),
            'created_by' => Yii::t('app', 'Created By'),
            'dat_modified' => Yii::t('app', 'Dat Modified'),
            'modfied_by' => Yii::t('app', 'Modfied By'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getMatchSeason()
    {
        return $this->hasOne(MatchSeason::className(), ['int_match_season_id' => 'int_match_season_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPlayerOfTheMatch()
    {
        return $this->hasOne(Player::className(), ['int_player_id' => 'int_player_of_the_match_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTeam1()
    {
        return $this->hasOne(Team::className(), ['int_team_id' => 'int_team1_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTeam2()
    {
        return $this->hasOne(Team::className(), ['int_team_id' => 'int_team2_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTossWinnerTeam()
    {
        return $this->hasOne(Team::className(), ['int_team_id' => 'int_toss_winner_team_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getVenue()
    {
        return $this->hasOne(Venue::className(), ['int_venue_id' => 'int_venue_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getWinnerTeam()
    {
        return $this->hasOne(Team::className(), ['int_team_id' => 'int_winner_team_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPlayerMatches()
    {
        return $this->hasMany(PlayerMatch::className(), ['int_match_id' => 'int_match_id']);
    }
}
