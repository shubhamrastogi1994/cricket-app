<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * MatchSearch represents the model behind the search form of `backend\models\Match`.
 */
class MatchSearch extends Match
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['int_match_id', 'int_team1_id', 'int_team2_id', 'int_venue_id', 'int_match_season_id', 'int_toss_winner_team_id', 'int_toss_decision_type', 'int_win_type', 'int_win_margin', 'int_player_of_the_match_id', 'int_winner_team_id', 'created_by', 'modfied_by'], 'integer'],
            [['dat_match', 'dat_created', 'dat_modified'], 'safe'],
//            [['dat_match'], 'date','format'=>'d-m-Y'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Match::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if(!empty($this->dat_match)){
            $this->dat_match = date('Y-m-d',strtotime($this->dat_match));
        }
//            echo '<pre>';print_r($this);exit;
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
//            $query->where('0=1');
            return $dataProvider;
        }


        // grid filtering conditions
        $query->andFilterWhere([
            'int_match_id' => $this->int_match_id,
            'int_team1_id' => $this->int_team1_id,
            'int_team2_id' => $this->int_team2_id,
            'dat_match' => $this->dat_match,
            'int_venue_id' => $this->int_venue_id,
            'int_match_season_id' => $this->int_match_season_id,
            'int_toss_winner_team_id' => $this->int_toss_winner_team_id,
            'int_toss_decision_type' => $this->int_toss_decision_type,
            'int_win_type' => $this->int_win_type,
            'int_win_margin' => $this->int_win_margin,
            'int_player_of_the_match_id' => $this->int_player_of_the_match_id,
            'int_winner_team_id' => $this->int_winner_team_id,
            'dat_created' => $this->dat_created,
            'created_by' => $this->created_by,
            'dat_modified' => $this->dat_modified,
            'modfied_by' => $this->modfied_by,
        ]);

        return $dataProvider;
    }
}
