<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Player */

$this->title = $model->int_player_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Players'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="player-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->int_player_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->int_player_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'int_player_id',
            'txt_first_name',
            'txt_middle_name',
            'txt_last_name',
            'txt_name',
            'txt_short_name',
            'txt_image_url:url',
            'txt_jersey_number',
            'int_team_id',
            'dat_dob',
            'int_batting_style_type',
            'int_balling_style_type',
            'dat_created',
            'created_by',
            'dat_modified',
            'modfied_by',
        ],
    ]) ?>

</div>
