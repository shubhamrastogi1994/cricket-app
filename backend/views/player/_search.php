<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PlayerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="player-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'int_player_id') ?>

    <?= $form->field($model, 'txt_first_name') ?>

    <?= $form->field($model, 'txt_middle_name') ?>

    <?= $form->field($model, 'txt_last_name') ?>

    <?= $form->field($model, 'txt_name') ?>

    <?php // echo $form->field($model, 'txt_short_name') ?>

    <?php // echo $form->field($model, 'txt_image_url') ?>

    <?php // echo $form->field($model, 'txt_jersey_number') ?>

    <?php // echo $form->field($model, 'int_team_id') ?>

    <?php // echo $form->field($model, 'dat_dob') ?>

    <?php // echo $form->field($model, 'int_batting_style_type') ?>

    <?php // echo $form->field($model, 'int_balling_style_type') ?>

    <?php // echo $form->field($model, 'dat_created') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'dat_modified') ?>

    <?php // echo $form->field($model, 'modfied_by') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
