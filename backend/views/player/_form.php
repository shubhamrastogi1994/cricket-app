<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Player */
/* @var $form yii\widgets\ActiveForm */
$arrTeams = \yii\helpers\ArrayHelper::map(\backend\models\Team::find()->all(),'int_team_id','txt_name');
$arrBattingType = \yii\helpers\ArrayHelper::map(\backend\models\Category::find()->andWhere(['int_category_type_id'=> 1])->andWhere('int_type_id is not null')->all(),'int_type_id','txt_name');
$arrBallingType = \yii\helpers\ArrayHelper::map(\backend\models\Category::find()->andWhere(['int_category_type_id'=> 2])->andWhere('int_type_id is not null')->all(),'int_type_id','txt_name');
?>

<div class="container">
    <?php $form = ActiveForm::begin([
        'id' => 'category-form',
        'enableAjaxValidation' => true,
        'fieldConfig' => [
            'template' => "{label}{input}{error}",
        ],
    ]); ?>
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                <?= $this->title = ($model->isNewRecord ? Yii::t('app','Create Team'):  Yii::t('app', 'Update Team: {name}', [
                    'name' => $model->txt_name,
                ]) ) ?>
            </div>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-sm-6">
                    <?= $form->field($model, 'txt_first_name')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'txt_middle_name')->textInput(['maxlength' => true]) ?>

                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'txt_last_name')->textInput(['maxlength' => true]) ?>

                </div>
                <div class="col-sm-6">    <?= $form->field($model, 'txt_short_name')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-sm-6">    <?= $form->field($model, 'txt_image_url')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-sm-6">    <?= $form->field($model, 'txt_jersey_number')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-sm-6">    <?= $form->field($model, 'int_team_id')->dropDownList($arrTeams,['prompt'=>'']) ?>
                </div>
                <div class="col-sm-6">    <?= $form->field($model, 'dat_dob')->textInput(['type'=>'date']) ?>
                </div>
                <div class="col-sm-6">    <?= $form->field($model, 'int_batting_style_type')->dropDownList($arrBattingType,['prompt'=>'']) ?>
                </div>
                <div class="col-sm-6">    <?= $form->field($model, 'int_balling_style_type')->dropDownList($arrBallingType,['prompt'=>'']) ?>
                </div>

            </div>
            <div class="row">
                <div class="col-sm-4">
                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                    <?php if($model->isNewRecord){
                        echo Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ;
                    }else{
                        echo Html::a(Yii::t('app', 'Cancel'),\yii\helpers\Url::to(['index']), ['class' => 'btn btn-default']);
                    }
                    ?>
                </div>
            </div>


        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
