<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PlayerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Players');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">
    <div class="card mt-3 card-default">
        <div class="card-header">
            <div class="card-title">
                <?= Html::encode($this->title) ?>
            </div>
            <div class="pull-right">
                <?php
                echo Html::a(Yii::t('app', 'Add'), ['create'], ['class' => 'btn btn-success btn-sm btn-flat']);
                echo Html::a(Yii::t('app', 'Back to List'), ['index'], ['class' => 'btn btn-default btn-sm btn-flat']);
                ?>
            </div>
        </div>
        <div class="card-block">

            <?php Pjax::begin(); ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

//                    'int_player_id',
//                    'txt_first_name',
//                    'txt_middle_name',
//                    'txt_last_name',
                    [
                        'attribute'=>'txt_image_url',
                        'format' => 'image',
                        'value' => function ($data) {
                            return $data->txt_image_url;
                        },],
                    'txt_name',
                    //'txt_short_name',
//                    'txt_image_url:url',
                    //'txt_jersey_number',
                    'team.txt_name',
                    //'dat_dob',
                    //'int_batting_style_type',
                    //'int_balling_style_type',
                    //'dat_created',
                    //'created_by',
                    //'dat_modified',
                    //'modfied_by',

                    [
                            'class' => 'yii\grid\ActionColumn',
                        'buttons' => [
                            'delete' => function ($url, $model, $key) {
                                return Html::a('<i class="glyphicon glyphicon-trash"></i>', $url, ['title' => Yii::t('app', 'Delete'), 'aria' => ['label' => Yii::t('app', 'Delete')], 'data' => ['confirm' => Yii::t('app', "Are you sure you want to delete this item?"), 'method' => 'post', 'pjax' => 0]]);

                            },
                            'update' => function ($url, $model, $key) {
                                return Html::a('<i class="glyphicon glyphicon-edit"></i>', $url, ['title'=>Yii::t('app','Update')]);

                            },

                        ],
                        'template' => '{update} {delete}',],
                ],
            ]); ?>

            <?php Pjax::end(); ?>

        </div>
    </div>
</div>
