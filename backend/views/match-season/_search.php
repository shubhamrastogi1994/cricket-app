<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MatchSeasonSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="match-season-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'int_match_season_id') ?>

    <?= $form->field($model, 'txt_name') ?>

    <?= $form->field($model, 'txt_short_name') ?>

    <?= $form->field($model, 'dat_created') ?>

    <?= $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'dat_modified') ?>

    <?php // echo $form->field($model, 'modfied_by') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
