<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\MatchSeason */

$this->title = Yii::t('app', 'Update Match Season: {name}', [
    'name' => $model->int_match_season_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Match Seasons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->int_match_season_id, 'url' => ['view', 'id' => $model->int_match_season_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="match-season-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
