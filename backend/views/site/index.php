<?php

/* @var $this yii\web\View */

$this->title = 'Cricket App';

use yii\helpers\Url; ?>
<div class="container">
    <div class="row">
        <div class="col-sm-4">
            <div class="card">
                <div class="card-header">
                    <div class="card-title"><a href="<?= Url::to(['/team'])?>"> <?= Yii::t('app','Teams')?></a>  </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card">
                <div class="card-header">
                    <div class="card-title"><a href="<?= Url::to(['/player'])?>"> <?= Yii::t('app','Players')?></a>  </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card">
                <div class="card-header">
                    <div class="card-title"><a href="<?= Url::to(['/match'])?>"> <?= Yii::t('app','Fixtures')?></a>  </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-5">
    <div class="mx-auto col-sm-4">
        <div class="card">
            <div class="card-header">
                <div class="card-title"><a href="<?= Url::to(['/points'])?>"> <?= Yii::t('app','Points Table')?></a>  </div>
            </div>
        </div>
    </div>
</div>

</div>