<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MatchSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="match-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'int_match_id') ?>

    <?= $form->field($model, 'int_team1_id') ?>

    <?= $form->field($model, 'int_team2_id') ?>

    <?= $form->field($model, 'dat_match') ?>

    <?= $form->field($model, 'int_venue_id') ?>

    <?php // echo $form->field($model, 'int_match_season_id') ?>

    <?php // echo $form->field($model, 'int_toss_winner_team_id') ?>

    <?php // echo $form->field($model, 'int_toss_decision_type') ?>

    <?php // echo $form->field($model, 'int_win_type') ?>

    <?php // echo $form->field($model, 'int_win_margin') ?>

    <?php // echo $form->field($model, 'int_player_of_the_match_id') ?>

    <?php // echo $form->field($model, 'int_winner_team_id') ?>

    <?php // echo $form->field($model, 'dat_created') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'dat_modified') ?>

    <?php // echo $form->field($model, 'modfied_by') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
