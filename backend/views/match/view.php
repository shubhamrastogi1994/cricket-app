<?php

use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Match */

$this->title = $model->int_match_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Matches'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
?>
<div class="match-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->int_match_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->int_match_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'int_match_id',
            'int_team1_id',
            'int_team2_id',
            'dat_match',
            'int_venue_id',
            'int_match_season_id',
            'int_toss_winner_team_id',
            'int_toss_decision_type',
            'int_win_type',
            'int_win_margin',
            'int_player_of_the_match_id',
            'int_winner_team_id',
            'dat_created',
            'created_by',
            'dat_modified',
            'modfied_by',
        ],
    ]) ?>

</div>
