<?php

use backend\models\MatchSeason;
use backend\models\Team;
use backend\models\Venue;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MatchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Matches');
$this->params['breadcrumbs'][] = $this->title;
$arrTeams = ArrayHelper::map(Team::find()->all(),'int_team_id','txt_name');
$arrVenues = ArrayHelper::map(Venue::find()->all(),'int_venue_id','txt_name');
$arrMatchSeason = ArrayHelper::map(MatchSeason::find()->all(),'int_match_season_id','txt_name');
if (!empty($searchModel['dat_match'])) {
    $searchModel->dat_match = Yii::$app->formatter->asDatetime($searchModel['dat_match'], "php:d-M-Y");
}
?>
<div class="category-index">
    <div class="card mt-3 card-default">
        <div class="card-header">
            <div class="card-title">
                <?= Html::encode($this->title) ?>
            </div>
            <div class="pull-right">
                <?php
                echo Html::a(Yii::t('app', 'Add'), ['create'], ['class' => 'btn btn-success btn-sm btn-flat']);
                echo Html::a(Yii::t('app', 'Back to List'), ['index'], ['class' => 'btn btn-default btn-sm btn-flat']);
                ?>
            </div>
        </div>
        <div class="card-block">
            <?php Pjax::begin(); ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute'=>'dat_match',
                        'format' => ['date','dd-MMM-YYYY'],
                        'filter' => \kartik\date\DatePicker::widget(['model' => $searchModel,'attribute'=>'dat_match','size' => 'sm', 'removeButton' => false, ]),
                    ],
                    [
                        'attribute'=>'int_venue_id',
                        'value' => 'venue.txt_name',
                        'filter' => $arrVenues,
                    ],[
                        'attribute'=>'int_team1_id',
                        'value' => 'team1.txt_name',
                        'filter' => $arrTeams,
                    ],[
                        'attribute'=>'int_team2_id',
                        'value' => 'team2.txt_name',
                        'filter' => $arrTeams,
                    ],[
                        'attribute'=>'int_match_season_id',
                        'value' => 'matchSeason.txt_name',
                        'filter' => $arrMatchSeason,
                    ],[
                        'attribute'=>'int_winner_team_id',
                        'value' => 'winnerTeam.txt_name',
                        'filter' => $arrTeams,
                    ],

                    //'int_toss_winner_team_id',
                    //'int_toss_decision_type',
                    //'int_win_type',
                    //'int_win_margin',
                    //'int_player_of_the_match_id',
                    //'int_winner_team_id',
                    //'dat_created',
                    //'created_by',
                    //'dat_modified',
                    //'modfied_by',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'buttons' => [
                            'delete' => function ($url, $model, $key) {
                                return Html::a('<i class="glyphicon glyphicon-trash"></i>', $url, ['title' => Yii::t('app', 'Delete'), 'aria' => ['label' => Yii::t('app', 'Delete')], 'data' => ['confirm' => Yii::t('app', "Are you sure you want to delete this item?"), 'method' => 'post', 'pjax' => 0]]);

                            },
                            'update' => function ($url, $model, $key) {
                                return Html::a('<i class="glyphicon glyphicon-edit"></i>', $url, ['title'=>Yii::t('app','Update'),]);

                            },

                        ],
                        'template' => '{update} {delete}',],
                ],
            ]); ?>

            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
