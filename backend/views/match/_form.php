<?php

use backend\models\Category;
use backend\models\MatchSeason;
use backend\models\Player;
use backend\models\Team;
use backend\models\Venue;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Match */
/* @var $form yii\widgets\ActiveForm */
$arrTeams = ArrayHelper::map(Team::find()->all(),'int_team_id','txt_name');
$arrVenues = ArrayHelper::map(Venue::find()->all(),'int_venue_id','txt_name');
$arrMatchSeason = ArrayHelper::map(MatchSeason::find()->all(),'int_match_season_id','txt_name');

$arrTossDecisionType = ArrayHelper::map(Category::find()->andWhere(['int_category_type_id'=> 1])->andWhere('int_type_id is not null')->all(),'int_type_id','txt_name');
$arrWinType = ArrayHelper::map(Category::find()->andWhere(['int_category_type_id'=> 1])->andWhere('int_type_id is not null')->all(),'int_type_id','txt_name');
$arrPlayers = [];
if(!empty($model->int_team1_id)){

    $arrPlayers = ArrayHelper::map(Player::find()->andWhere(['in','int_team_id',[$model->int_team1_id,$model->int_team2_id]])->all(),'int_player_id','txt_name');

}
?>

<div class="container">
    <?php $form = ActiveForm::begin([
        'id' => 'category-form',
        'enableAjaxValidation' => true,
        'fieldConfig' => [
            'template' => "{label}{input}{error}",
        ],
    ]); ?>
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                <?= $model->isNewRecord ? Yii::t('app','Create Match'):  Yii::t('app', 'Update Match: {name}', [
                    'name' => $model->dat_match,
                ])  ?>
            </div>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-sm-6">
                    <?= $form->field($model, 'dat_match')->textInput(['type'=>'date']) ?>

                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'int_team1_id')->dropDownList($arrTeams,['prompt'=>'']) ?>

                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'int_team2_id')->dropDownList($arrTeams,['prompt'=>'']) ?>

                </div>
                <div class="col-sm-6">    <?= $form->field($model, 'int_venue_id')->dropDownList($arrVenues,['prompt'=>'']) ?>
                </div>
                <div class="col-sm-6">    <?= $form->field($model, 'int_match_season_id')->dropDownList($arrMatchSeason,['prompt'=>'']) ?>
                </div>
                <div class="col-sm-6">    <?= $form->field($model, 'int_toss_winner_team_id')->dropDownList($arrTeams,['prompt'=>'']) ?>
                </div>
                <div class="col-sm-6">    <?= $form->field($model, 'int_toss_decision_type')->dropDownList($arrTossDecisionType,['prompt'=>'']) ?>
                </div>
                <div class="col-sm-6">    <?= $form->field($model, 'int_win_type')->dropDownList($arrWinType,['prompt'=>'']) ?>
                </div>
                <div class="col-sm-6">    <?= $form->field($model, 'int_win_margin')->textInput() ?>
                </div>
                <div class="col-sm-6">    <?= $form->field($model, 'int_player_of_the_match_id')->dropDownList($arrPlayers,['prompt'=>'']) ?>
                </div>
                <div class="col-sm-6">    <?= $form->field($model, 'int_winner_team_id')->dropDownList($arrTeams,['prompt'=>'']) ?>
                </div>
                <div class="col-sm-6"></div>
                <div class="col-sm-6"></div>
                <div class="col-sm-6"></div>
                <div class="col-sm-6"></div>
                <div class="col-sm-6"></div>

            </div>
            <div class="row">
                <div class="col-sm-4">
                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                    <?php if($model->isNewRecord){
                        echo Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ;
                    }else{
                        echo Html::a(Yii::t('app', 'Cancel'), Url::to(['index']), ['class' => 'btn btn-default']);
                    }
                    ?>
                </div>
            </div>


        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
