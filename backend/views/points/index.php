<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PlayerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $intMatchSeasonId array  */

$this->title = Yii::t('app', 'Points Table');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">
    <div class="card mt-3 card-default">
        <div class="card-header">
            <div class="card-title">
                <?= Html::encode($this->title) ?>
            </div>

        </div>
        <div class="card-block">

            <?php Pjax::begin(); ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
//                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],


                    'txt_name',
                    [
                        'attribute'=>'Played',
                        'value'=>function($model) use($intMatchSeasonId){
                        return \backend\models\Match::find()
                            ->andWhere("int_team1_id = {$model['int_team_id']} or int_team2_id = {$model['int_team_id']}")
                            ->andWhere(['int_match_season_id'=>$intMatchSeasonId])->count();
                        }
                    ],
                    [
                        'attribute'=>'Win',
                        'value'=>function($model) use($intMatchSeasonId){
                            return \backend\models\Match::find()
                                ->andWhere("int_winner_team_id = {$model['int_team_id']} or int_winner_team_id = {$model['int_team_id']}")
                                ->andWhere(['int_match_season_id'=>$intMatchSeasonId])->count();
                        }
                    ],
                    [
                        'attribute'=>'Lost',
                        'value'=>function($model) use($intMatchSeasonId){
                            return  \backend\models\Match::find()
                                ->andWhere("int_team1_id = {$model['int_team_id']} or int_team2_id = {$model['int_team_id']}")
                                ->andWhere(['int_match_season_id'=>$intMatchSeasonId])->count() -  \backend\models\Match::find()
                                    ->andWhere("int_winner_team_id = {$model['int_team_id']} or int_winner_team_id = {$model['int_team_id']}")
                                    ->andWhere(['int_match_season_id'=>$intMatchSeasonId])->count();
                        }
                    ],
                    [
                        'attribute'=>'Points',
                        'value'=>function($model) use($intMatchSeasonId){
                            return  \backend\models\Match::find()
                                    ->andWhere("int_winner_team_id = {$model['int_team_id']} or int_winner_team_id = {$model['int_team_id']}")
                                    ->andWhere(['int_match_season_id'=>$intMatchSeasonId])->count() *2;
                        }
                    ]

                ],
            ]); ?>

            <?php Pjax::end(); ?>

        </div>
    </div>
</div>
