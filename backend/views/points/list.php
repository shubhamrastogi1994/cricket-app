<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MatchSeasonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Points Table for Season');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">
    <div class="card mt-3 card-default">
        <div class="card-header">
            <div class="card-title">
                <?= Html::encode($this->title) ?>
            </div>
        </div>
        <div class="card-block">

            <?php Pjax::begin(); ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

//                    'int_match_season_id',

                    [
                        'attribute' => 'txt_name',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return Html::a($data['txt_name'], Url::to(['/points/list', 'intMatchSeasonId' => $data['int_match_season_id']]));
                        },],
                    'txt_short_name',
//                    'dat_created',
//                    'created_by',
                    //'dat_modified',
                    //'modfied_by',

                ],
            ]); ?>

            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
