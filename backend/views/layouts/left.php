<?php

use backend\assets\Menu;

?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/avatar5.png" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->username?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

  

        <?php


        echo Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    // ['label' => 'Academics', 'icon' => 'calendar-o','options' => ['class' => 'header']],
                    [
                        'label' => 'Team Management',
                        'icon' => 'graduation-cap',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Team', 'icon' => 'calendar', 'url' => ['/team']],
                            ['label' => 'Player', 'icon' => 'graduation-cap', 'url' => ['/player']],
                            ['label' => 'Match Season', 'icon' => 'info-circle', 'url' => ['/match-season']],
                            ['label' => 'Match', 'icon' => 'info-circle', 'url' => ['/match']],
                            ['label' => 'Points Table', 'icon' => 'book', 'url' => ['/points']],
                            ['label' => 'Country', 'icon' => 'sitemap', 'url' => ['/country']],
                            ['label' => 'Venue', 'icon' => 'sitemap', 'url' => ['/venue']],
                            ['label' => 'Categories', 'icon' => 'share-alt', 'url' => ['/category']],
                        ],
                    ],

                    ['label' => 'Logout', 'url' => ['site/logout'], 'data'=>['method'=>'post'], 'visible' => !Yii::$app->user->isGuest],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],

                ],
            ]
        ) ?>

    </section>

</aside>
