<?php

use backend\models\Category;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Category */
/* @var $form yii\widgets\ActiveForm */
$arrParams = Yii::$app->request->queryParams;

?>
<div class="container">
<?php $form = ActiveForm::begin([
    'id' => 'category-form',
    'enableAjaxValidation' => true,
    'fieldConfig' => [
        'template' => "{label}{input}{error}",
    ],
]); ?>
<div class="card">
    <div class="card-header">
        <div class="card-title">
            <?= $model->isNewRecord ? Yii::t('app','Create Category'):  Yii::t('app', 'Update Category: {name}', [
                'name' => $model->txt_name,
            ])  ?>
        </div>
    </div>
    <div class="card-block">
        <div class="row">
            <div class="col-sm-6">
                <?php
                if(!empty($arrParams['intCategoryId'])){
                    $arrCategory = Category::find()->andWhere(['int_category_type_id'=>$arrParams['intCategoryId']])->all();
                    $arrCategory = ArrayHelper::map($arrCategory,'int_category_type_id','txt_name');
                    echo $form->field($model, 'int_category_type_id')->dropDownList($arrCategory, ['prompt' => '']);
                }else{
                    echo $form->field($model, 'int_category_type_id')->textInput(['maxlength' => true,'type'=>'number']);
                }
                ?>
            </div>

            <div class="col-sm-6">

                    <?= $form->field($model, 'int_type_id')->textInput(['maxlength' => true, 'type' => 'number']); ?>

            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'txt_name')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                <?php if($model->isNewRecord){
                    echo Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ;
                }else{
                    echo Html::a(Yii::t('app', 'Cancel'), Url::to(['index']), ['class' => 'btn btn-default']);
                }
                ?>
            </div>
        </div>


    </div>
</div>
<?php ActiveForm::end(); ?>
</div>