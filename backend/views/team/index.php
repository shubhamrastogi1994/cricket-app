<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TeamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Teams');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">
    <div class="card mt-3 card-default">
        <div class="card-header">
            <div class="card-title">
                <?= Html::encode($this->title) ?>
            </div>
            <div class="pull-right">
                <?php
                echo Html::a(Yii::t('app', 'Add'), ['create'], ['class' => 'btn btn-success btn-sm btn-flat', 'data' => ['target' => '#globalModal', 'toggle' => 'modal', 'modal-size' => 'modal-lg']]);
                echo Html::a(Yii::t('app', 'Back to List'), ['index'], ['class' => 'btn btn-default btn-sm btn-flat']);
                ?>
            </div>
        </div>
        <div class="card-block">
            <?php Pjax::begin(); ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

//                    'int_team_id',
                    [
                        'attribute'=>'txt_logo_url',
                        'format' => 'image',
                        'value' => function ($data) {
                            return $data->txt_logo_url;
                        },],
                    [
                        'attribute' => 'txt_name',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return Html::a($data['txt_name'], \yii\helpers\Url::to(['/player', 'PlayerSearch[int_team_id]' => $data['int_team_id']]));
                        },],

                    'txt_short_name',

//                    'txt_club_state',
                    [
                        'label'=>Yii::t('app','Country'),
                        'attribute'=>'intCountry.txt_name'
                    ],
                    //'dat_created',
                    //'created_by',
                    //'dat_modified',
                    //'modfied_by',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'buttons' => [
                            'delete' => function ($url, $model, $key) {
                                return Html::a('<i class="glyphicon glyphicon-trash"></i>', $url, ['title' => Yii::t('app', 'Delete'), 'aria' => ['label' => Yii::t('app', 'Delete')], 'data' => ['confirm' => Yii::t('app', "Are you sure you want to delete this item?"), 'method' => 'post', 'pjax' => 0]]);

                            },
                            'update' => function ($url, $model, $key) {
                                return Html::a('<i class="glyphicon glyphicon-edit"></i>', $url, ['title'=>Yii::t('app','Update'),'data'=>['target'=> '#globalModal','toggle'=>'modal','modal-size'=>'modal-lg']]);

                            },

                        ],
                        'template' => '{update} {delete}',],
                ],
            ]); ?>

            <?php Pjax::end(); ?>
        </div>
    </div>
</div>