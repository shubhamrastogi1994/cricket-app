<?php

namespace backend\controllers;

use app\models\User;
use SplFileInfo;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * CityController implements the CRUD actions for City model.
 */
class BaseController extends Controller
{
    public $modelClass;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    // public function actions()
    // {
    //     return [
    //         'toggle' => [
    //             'class' => ToggleAction::className(),
    //             'modelClass' => $this->modelClass,
    //             'attribute' => 'ysn_status',
    //             // Uncomment to enable flash messages
    //             'setFlash' => true,
    //             'flashSuccess' => Yii::t('app', "Academic year udpated")
    //         ],
    //     ];
    // }

    /**
     * Lists all City models.
     * @param Object $model
     * @param Object $searchModel
     * @param array $arrExtraParams
     * @return mixed
     */
    protected function getIndex($model, $searchModel, $arrExtraParams = [])
    {
        $view = 'index';
        if (!empty(Yii::$app->request->referrer)) {
            $redirectUrl = Yii::$app->request->referrer;
        } else {
            $redirectUrl = ['index'];
        }
        if (!empty($arrExtraParams['view'])) {
            $view = $arrExtraParams['view'];
        }
        if (!empty($arrExtraParams['redirectUrl'])) {
            $redirectUrl = $arrExtraParams['redirectUrl'];
        }

        $arrParams = Yii::$app->request->queryParams;
        $arrPost = Yii::$app->request->post();
        $dataProvider = $searchModel->search($arrParams);
        if (!empty($arrParams['id'])) {
            $model = $this->findModel($arrParams['id'], $model->className());
        }

        $arrCols = ArrayHelper::getColumn($model->getTableSchema()->columns, 'dbType');
        $model->loadDefaultValues();

        if ($model->load($arrPost) && Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return array_merge(ActiveForm::validate($model));
        }
        if ($model->load($arrPost)) {
            if ($model->validate()) {
                $model->save();
            } else {
                echo '<pre>';
                print_r($model);
                exit;
            }

            Yii::$app->session->setFlash('success', 'Record saved successfully.');
            return $this->redirect($redirectUrl);
        }

        $model->handleDate($model);
        $searchModel->handleDate($searchModel);
        $arrRenderParams = [];
        if (!empty($arrExtraParams['viewParams'])) {
            $arrRenderParams = $arrExtraParams['viewParams'];
        }

        $arrRenderParams = ArrayHelper::merge($arrRenderParams, [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
        $render = $this->render($view, $arrRenderParams);
        if (!empty($arrExtraParams['renderAjax'])) {
            $render = $this->renderAjax($view, $arrRenderParams);
        }

        return $render;
    }


    /**
     * Creates a new Country model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @param Object $model
     * @param Object $searchModel
     * @param array $arrExtraParams
     * @return mixed
     */
    protected function getCreate($model, $searchModel, $arrExtraParams = [])
    {
        $view = 'index';
        $redirectUrl = ['index'];
        if (!empty($arrExtraParams['view'])) {
            $view = $arrExtraParams['view'];
        }

        $arrParams = Yii::$app->request->queryParams;
        $arrPost = Yii::$app->request->post();
        $dataProvider = $searchModel->search($arrParams);
        if (!empty($arrParams['id'])) {
            $model = $this->findModel($arrParams['id'], $model->className());
        }
        if (!empty($arrExtraParams['redirectUrl'])) {
            $redirectUrl = $arrExtraParams['redirectUrl'];
        }

        $model->loadDefaultValues();
        $arrCols = ArrayHelper::getColumn($model->getTableSchema()->columns, 'dbType');
        if (array_key_exists('int_org_id', $arrCols) && !empty(Yii::$app->user->identity->int_org_id)) {
            $model->int_org_id = Yii::$app->user->identity->int_org_id;
        }
        if ($model->load($arrPost) && Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return array_merge(ActiveForm::validate($model));
        }
        if ($model->load($arrPost)) {

            if ($model->validate()) {
//                echo "<pre>";print_r($model);exit;
                $model->save();
            } else {
                echo '<pre>';
                print_r($model->errors);
                exit;
            }

            Yii::$app->session->setFlash('success', 'Record saved successfully.');
            return $this->redirect($redirectUrl);
        }

        $model->handleDate($model);

        $arrRenderParams = [];
        if (!empty($arrExtraParams['viewParams'])) {
            $arrRenderParams = $arrExtraParams['viewParams'];
        }

        $arrRenderParams = ArrayHelper::merge($arrRenderParams, [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
        $render = $this->render($view, $arrRenderParams);
        if (!empty($arrExtraParams['renderAjax'])) {
            $render = $this->renderAjax($view, $arrRenderParams);
        }
        return $render;
    }


    /**
     * Updates an existing Country model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param Object $model
     * @param Object $searchModel
     * @param array $arrExtraParams
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function getUpdate($model, $searchModel, $arrExtraParams = [])
    {
        $view = 'index';
        if (!empty(Yii::$app->request->referrer)) {
            $redirectUrl = Yii::$app->request->referrer;
        } else {
            $redirectUrl = ['index'];
        }
        if (!empty($arrExtraParams['view'])) {
            $view = $arrExtraParams['view'];
        }
        if (!empty($arrExtraParams['redirectUrl'])) {
            $redirectUrl = $arrExtraParams['redirectUrl'];
        }

        $arrParams = Yii::$app->request->queryParams;
        $arrPost = Yii::$app->request->post();
        $dataProvider = $searchModel->search($arrParams);
        if (!empty($arrParams['id'])) {
            $model = $this->findModel($arrParams['id'], $model->className());
        }

        $model->loadDefaultValues();
        $arrCols = ArrayHelper::getColumn($model->getTableSchema()->columns, 'dbType');
        if (array_key_exists('int_org_id', $arrCols) && !empty(Yii::$app->user->identity->int_org_id)) {
            $model->int_org_id = Yii::$app->user->identity->int_org_id;
        }
        if ($model->load($arrPost) && Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return array_merge(ActiveForm::validate($model));
        }
        if ($model->load($arrPost)) {

            if ($model->validate()) {

                $model->save();
            } else {
                echo '<pre>';
                print_r($model->errors);
                exit;
            }

            Yii::$app->session->setFlash('success', 'Record saved successfully.');
            return $this->redirect($redirectUrl);
        }

        $model->handleDate($model);
        $arrRenderParams = [];
        if (!empty($arrExtraParams['viewParams'])) {
            $arrRenderParams = $arrExtraParams['viewParams'];
        }

        $arrRenderParams = ArrayHelper::merge($arrRenderParams, [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
        $render = $this->render($view, $arrRenderParams);
        if (!empty($arrExtraParams['renderAjax'])) {
            $render = $this->renderAjax($view, $arrRenderParams);
        }
        return $render;
    }


    /**
     * Deletes an existing Country model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param string $modelClass
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function getDelete($id, $modelClass, $arrExtraParams = [])
    {
        /* @var $model ActiveRecord */
        if (!empty(Yii::$app->request->referrer)) {
            $redirectUrl = Yii::$app->request->referrer;
        } else {
            $redirectUrl = ['index'];
        }


        if (!empty($arrExtraParams['redirectUrl'])) {
            $redirectUrl = $arrExtraParams['redirectUrl'];
        }
        try {

            $model = $this->findModel($id, $modelClass);
            $markDelete = true;
            $arrRefrences = Yii::$app->db->createCommand("SELECT 
                                                              * 
                                                        FROM
                                                            information_schema.KEY_COLUMN_USAGE
                                                        WHERE
                                                            CONSTRAINT_SCHEMA = DATABASE()
                                                                AND REFERENCED_TABLE_SCHEMA =  DATABASE() 
                                                                AND REFERENCED_TABLE_NAME = :current_table_name
                                                                AND REFERENCED_COLUMN_NAME = :key")
                ->bindValues([':current_table_name' => $modelClass::tableName(), ':key' => $modelClass::primaryKey()[0]])->queryAll();
            if (!empty($arrRefrences)) {
                foreach ($arrRefrences as $recRefrences) {
                    $arrTable = Yii::$app->db->createCommand('SELECT count(*) as count from ' . $recRefrences['TABLE_NAME'] . ' where ' . $recRefrences['COLUMN_NAME'] . ' = ' . $id)->queryOne();
                    if ($arrTable['count'] != 0) {
                        $markDelete = false;
                        break;
                    }
                }
            }

            if ($markDelete) {
                $model->delete();
                Yii::$app->session->setFlash('success', 'Record deleted successfully.');
            } else {
                Yii::$app->session->setFlash('error', 'You can not delete this record because it is used in another table.');
            }
        } catch (Exception $integrityException) {
            Yii::$app->session->setFlash('error', 'You can not delete this record because it is used in another table.');
        }
        return $this->redirect($redirectUrl);
    }

    // /**
    //  * Finds the $modelClass model based on its primary key value.
    //  * If the model is not found, a 404 HTTP exception will be thrown.
    //  * @param integer $id
    //  * @param string $modelClass
    //  * @return ActiveRecord the loaded model
    //  * @throws NotFoundHttpException if the model cannot be found
    //  */
    // protected function findModel($id, $modelClass)
    // {

    //     if (($model = $modelClass::findOne($id)) !== null) {
    //         return $model;
    //     }

    //     throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    // }

    protected function createDirectories($intOrganisationId)
    {
        $mode = 0766;
        if (!file_exists(Yii::$app->basePath . "\data")) {
            mkdir(Yii::$app->basePath . "\data", $mode);
        }
        if (!is_dir(Yii::$app->basePath . "/data")) {
            mkdir(Yii::$app->basePath . "/data", $mode);
        }
//        echo '<pre>';print_r(Yii::$app->basePath);exit;
        if (is_dir(Yii::$app->basePath . "/data")) {
            $baseDir = "/data/" . $intOrganisationId;
//                echo $baseDir;exit;
            if (!file_exists(Yii::$app->basePath . $baseDir)) {
                mkdir(Yii::$app->basePath . $baseDir, $mode);
            }
            if (is_dir(Yii::$app->basePath . "{$baseDir}")) {
                //TODO
                if (!file_exists(Yii::$app->basePath . "{$baseDir}/logo")) {
                    mkdir(Yii::$app->basePath . "{$baseDir}/logo", $mode);
                }
                if (!file_exists(Yii::$app->basePath . "{$baseDir}/employee")) {
                    mkdir(Yii::$app->basePath . "{$baseDir}/employee", $mode);
                    if (is_dir(Yii::$app->basePath . "{$baseDir}/employee")) {
                        if (!file_exists(Yii::$app->basePath . "{$baseDir}/employee/docs")) {
                            mkdir(Yii::$app->basePath . "{$baseDir}/employee/docs", $mode);
                        }
                        if (!file_exists(Yii::$app->basePath . "{$baseDir}/employee/images")) {
                            mkdir(Yii::$app->basePath . "{$baseDir}/employee/images", $mode);
                        }
                    }
                }
                if (!file_exists(Yii::$app->basePath . "{$baseDir}/student")) {
                    mkdir(Yii::$app->basePath . "{$baseDir}/student", $mode);
                    if (is_dir(Yii::$app->basePath . "{$baseDir}/student")) {
                        if (!file_exists(Yii::$app->basePath . "{$baseDir}/student/docs")) {
                            mkdir(Yii::$app->basePath . "{$baseDir}/student/docs", $mode);
                        }
                        if (!file_exists(Yii::$app->basePath . "{$baseDir}/student/images")) {
                            mkdir(Yii::$app->basePath . "{$baseDir}/student/images", $mode);
                        }
                    }
                }
                if (!file_exists(Yii::$app->basePath . "{$baseDir}/notice")) {
                    mkdir(Yii::$app->basePath . "{$baseDir}/notice", $mode);
                }
                if (!file_exists(Yii::$app->basePath . "{$baseDir}/assignments")) {
                    mkdir(Yii::$app->basePath . "{$baseDir}/assignments", $mode);
                }
                if (!file_exists(Yii::$app->basePath . "{$baseDir}/import")) {
                    mkdir(Yii::$app->basePath . "{$baseDir}/import", $mode);
                    if (is_dir(Yii::$app->basePath . "{$baseDir}/import")) {
                        if (!file_exists(Yii::$app->basePath . "{$baseDir}/import/uploaded")) {
                            mkdir(Yii::$app->basePath . "{$baseDir}/import/uploaded", $mode);
                        }
                    }
                }
//                    if(!file_exists(Yii::$app->basePath.'{$baseDir}/event')){
//                        mkdir(Yii::$app->basePath.'{$baseDir}/event',$mode);
//                    }
//                    if(!file_exists(Yii::$app->basePath.'{$baseDir}//gallery')){
//                        mkdir(Yii::$app->basePath.'{$baseDir}/gallery',$mode);
//                    }


            }
        }
    }

    protected function storeDocument($model)
    {
        $mode = 0766;
        if (empty($model)) {
            return false;
        }
        /* @var $model Documents */
        $intStudentMasterId = NULL;
        $intEmployeeMasterId = NULL;
        $categoryModel = new Category();
        if (!empty($model->int_student_master_id)) {
            $intStudentMasterId = $model->int_student_master_id;
        }
        if (!empty($model->int_employee_master_id)) {
            $intEmployeeMasterId = $model->int_employee_master_id;
        }
        if (empty($intStudentMasterId) && empty($intEmployeeMasterId)) {
            return false;
        }
        if (!empty(Yii::$app->user->identity->int_org_id)) {
            $intOrganisationId = Yii::$app->user->identity->int_org_id;
        }
        if (!empty($_FILES[StringHelper::basename($model->classname())])) {
            $arrFiles = $_FILES[StringHelper::basename($model->classname())];
            $attribute = 'blb_document';
            if (!empty($arrFiles['type'][$attribute])) {
                $model->int_document_mime_category_id = $categoryModel->getCategoryByName($categoryModel->getMimeCategoryId(), $arrFiles['type'][$attribute]);
            }
            if (!empty($arrFiles['name'][$attribute])) {
                $info = new SplFileInfo($arrFiles['name'][$attribute]);
                $extension = $info->getExtension();
            }
            if (empty($extension)) {
                $extension = 'png';
            }
            $model->int_extension_category_id = $categoryModel->getCategoryByName($categoryModel->getMimeCategoryId(), $extension);

            if (!empty($arrFiles['tmp_name'][$attribute])) {
                $model->{$attribute} = base64_encode(file_get_contents($arrFiles['tmp_name'][$attribute]));
            }
//            echo '<pre>';print_r($model);exit;
        }


        if (!empty($intOrganisationId)) {
            $baseDir = "/data/" . $intOrganisationId;
            if (!empty($intStudentMasterId)) {
                $documentPath = Yii::$app->basePath . "{$baseDir}/student/docs/";
                $filename = time() . '_' . $model->int_student_master_id . '_' . $model->int_document_category_id . '_' . $model->int_deleted;
                $documentPath .= $filename;


                if (!empty($model->{$attribute})) {
                    file_put_contents($documentPath, $model->{$attribute});
                }

            }
            if (!empty($intEmployeeMasterId)) {
                $documentPath = Yii::$app->basePath . "{$baseDir}/employee/docs/";
                $filename = time() . '_' . $model->int_employee_master_id . '_' . $model->int_document_category_id . '_' . $model->int_deleted;
                $documentPath .= $filename;


                if (!empty($model->{$attribute})) {
                    file_put_contents($documentPath, $model->{$attribute});
                }

            }
            $model->txt_docs_path = $documentPath;
        }
        return $model;
    }
}
