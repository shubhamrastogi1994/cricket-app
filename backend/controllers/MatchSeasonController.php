<?php

namespace backend\controllers;

use backend\models\MatchSeason;
use backend\models\MatchSeasonSearch;
use backend\models\traitFunctions;
use Yii;
use yii\db\IntegrityException;
use yii\web\NotFoundHttpException;

/**
 * MatchSeasonController implements the CRUD actions for MatchSeason model.
 */
class MatchSeasonController extends BaseController
{
    use traitFunctions;

    /**
     * Lists all MatchSeason models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MatchSeasonSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MatchSeason model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MatchSeason model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MatchSeason();
        return $this->saveData($model);
    }

    /**
     * Updates an existing MatchSeason model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        return $this->saveData($model);
    }

    /**
     * Deletes an existing MatchSeason model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
            Yii::$app->session->setFlash('success', 'Record deleted successfully.');

        } catch (IntegrityException $ex) {
            Yii::$app->session->setFlash('error', 'Unable to delete record.');

        }


        return $this->redirect(['index']);
    }

    /**
     * Finds the MatchSeason model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MatchSeason the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MatchSeason::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
