<?php

namespace backend\controllers;

use backend\models\Category;
use backend\models\CategorySearch;
use Throwable;
use Yii;
use yii\db\IntegrityException;
use yii\db\StaleObjectException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends BaseController
{

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new CategorySearch();
        $model = new Category();
        $arrParams = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($arrParams);
        if (!empty($arrParams['intCategoryId'])) {
            $model->int_category_type_id = $arrParams['intCategoryId'];
        }

        return $this->render('index', compact('searchModel', 'dataProvider', 'model'));
    }


    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Category();
        $arrParams = Yii::$app->request->queryParams;
        $arrPost = Yii::$app->request->post();
        if (!empty($arrParams['intCategoryId'])) {
            $model->int_category_type_id = $arrParams['intCategoryId'];
        }

        if ($model->load($arrPost) && Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return array_merge(ActiveForm::validate($model));
        }
        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            Yii::$app->session->setFlash('success', 'Record saved successfully.');
            return $this->redirect(['index', 'intCategoryId' => $model->int_category_type_id]);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $arrPost = Yii::$app->request->post();
        if ($model->load($arrPost) && Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return array_merge(ActiveForm::validate($model));
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Record saved successfully.');
            return $this->redirect(['index', 'intCategoryId' => $model->int_category_type_id]);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
            Yii::$app->session->setFlash('success', 'Record deleted successfully.');

        } catch (IntegrityException $ex) {
            Yii::$app->session->setFlash('error', 'Unable to delete record.');

        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
