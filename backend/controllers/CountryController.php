<?php

namespace backend\controllers;

use backend\models\Country;
use backend\models\CountrySearch;
use backend\models\traitFunctions;
use Throwable;
use Yii;
use yii\db\IntegrityException;
use yii\db\StaleObjectException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * CountryController implements the CRUD actions for Country model.
 */
class CountryController extends Controller
{
    use traitFunctions;
    // $arrTo = [];
    /**
     * Lists all Country models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        $basePath = dirname(\Yii::$app->basePath);
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);

        if (!empty($_GET['create_new'])) {


            $handle = fopen($basePath . "/" . $_GET['create_new'] . ".csv", "r");
            while (($data = fgetcsv($handle)) !== FALSE) {
                if (strpos($data[0], '@') === false) {
                    continue;
                }

                if (!empty($data[0]) && !empty(trim($data[0]))) {
                    $email = trim($data[0]);
                    $email = trim($email, ',');
                    $email = trim($email, '.');
                    $email = trim($email, '@');
                    $email = trim($email, '`');
                    $email = strtolower($email);
                    // $email = str_replace(" ","",$email);
                    $email = str_replace("gmail,com", "gmail.com", $email);
                    $email = str_replace("gmail,.", "gmail.", $email);
                    $email = str_replace("gmail.,", "gmail.", $email);
                    $email = str_replace("gmailcom", "gmail.com", $email);
                    $email = str_replace("@gmail.com@gmail.com", "@gmail.com", $email);
                    $email = str_replace("g,mail", "gmail", $email);
                    $email = str_replace("@.", "@", $email);
                    $email = str_replace(".@", "@", $email);
                    $email = str_replace("`", "", $email);
                    $email = str_replace("'", "", $email);
                    $email = str_replace("@@", "@", $email);
                    $email = str_replace("\\", "", $email);
                    $email = str_replace("/", "", $email);
                    $email = str_replace(";", "", $email);
                    $email = str_replace("..", ".", $email);
                    $email = str_replace("(", ".", $email);
                    $email = str_replace(",", ".", $email);
                    $email = str_replace("]", "", $email);
                    $email = str_replace("[", "", $email);

                    if (preg_match("/@gmail$/", $email)) {
                        $email = str_replace("@gmail", "@gmail.com", $email);
                    }
                    if (strpos($email, "www.") === 0) {
                        $email = str_replace("www.", "", $email);
                    }
                    if (strpos($email, "@gma") !== false && strpos($email, "@gmail.com") === false) {
                        $email = explode('@gma', $email)[0] . "@gmail.com";
                    }
                    if (strpos($email, "@gmail.com") !== false && strpos(explode('@gmail.com', $email)[0], "@") !== false) {
                        $email = str_replace("@", "", explode('@gmail.com', $email)[0]) . "@gmail.com";
                    }

                    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                        $arrValidMails[trim($email)] = trim($email);
                        $arrAllMailsAfterFilter[] = [$data[0], $email, "Valid"];
                    } else {
                        $arrInvalidMails[trim($email)] = trim($email);
                        $arrAllMailsAfterFilter[] = [$data[0], $email, "InValid"];
                    }
                }
            }

            $file = fopen($basePath . "/"  . $_GET['state'] . "-valid.csv", "w");
            foreach ($arrValidMails as $line) {
                fputcsv($file, [$line]);
            }

            fclose($file);
            exit;
        }
        $handle = fopen($basePath . "/" ."mailsent.csv", "r");
        while (($data = fgetcsv($handle)) !== FALSE) {
            if (empty(trim($data[0]))) {
                continue;
            }
            $arrSentAlready[] = trim($data[0]);
        }
        $sendNew = $_GET['state'];

        $handle = fopen($basePath . "/" . $sendNew . "-valid.csv", "r");
        while (($data = fgetcsv($handle)) !== FALSE) {
            if (empty(trim($data[0]))) {
                continue;
            }
            if (strlen($data[0]) < 14) {
                continue;
            }
            $arrValidMails[] = trim($data[0]);
        }
        $counter = (!empty($_GET['counter']) ? $_GET['counter'] : 0);
        foreach ($arrValidMails as $email) {
            if(!in_array($email,$arrSentAlready)){
                $arrTo[] = $email;
                $counter++;
            }
            if($counter == 500){
                break;
            }
        }
        
        // Uttar Pradesh
        $strSubject = "Graphic Era University, Dehradun Early Registration Open for 2020 Session";
        $strMessage = <<<MSG
        <div dir="ltr"><span style="font-family:tahoma,geneva,sans-serif;font-size:14px">Dear Student,</span><br>
        <div class="gmail_quote">
            <div dir="ltr">
                <div dir="ltr">
                    <div class="gmail_quote">
                        <div dir="ltr">
                            <div dir="ltr">
                                <div dir="ltr">
                                    <div>
                                        <font face="Roboto, Tahoma, Verdana, Segoe, sans-serif">&nbsp;</font>
                                        <div style="font-family:Roboto,Tahoma,Verdana,Segoe,sans-serif"><span
                                                style="font-size:14px"><span style="font-family:tahoma,geneva,sans-serif">
                                                    <font color="#ff0000">Graphic Era University, Dehradun announces Early
                                                        Registration Open for
                                                        2020 Session:</font>
                                                </span></span></div>
                                        <div style="font-family:Roboto,Tahoma,Verdana,Segoe,sans-serif"><span
                                                style="font-size:14px"><span
                                                    style="font-family:tahoma,geneva,sans-serif"><br>
                                                </span></span></div>
                                        <div>
                                            <font face="tahoma, geneva, sans-serif" style="font-size:14px">Due to COVID-19,
                                                we understand that you are in a confuse state, as through what is going to
                                                happen to current
                                            </font>
                                            <font face="Roboto, Tahoma, Verdana, Segoe, sans-serif"><span
                                                    style="font-size:14px">Class Results and how the admission process will
                                                    take place for your&nbsp;preferred undergraduate courses.</span></font>
                                        </div>
                                        <div>
                                            <font face="Roboto, Tahoma, Verdana, Segoe, sans-serif"><span
                                                    style="font-size:14px"><br>
                                                </span></font>
                                        </div>
                                        <div>
                                            <font face="Roboto, Tahoma, Verdana, Segoe, sans-serif"><span
                                                    style="font-size:14px">To make it&nbsp;comfortable&nbsp;for &nbsp;you,
                                                    we are now taking early registrations on the basis of marks you scored
                                                    in Class X &amp; XI.&nbsp;</span></font>
                                        </div>
                                        <div><br>
                                        </div>
                                        <div>
                                            <font color="#000000" face="tahoma, geneva, sans-serif"><span
                                                    style="font-size:14px">The undergraduate program in
                                                    Engineering,Management,Commerce,Hospitality &amp; Hotel Management ,Life Sciences,&nbsp;</span><span
                                                        style="font-size:14px">Nursing</span><span
                                                        style="font-size:14px">,Computer
                                                        Applications and more.</span></font>
                                        </div>
                                        <div><span
                                                style="font-size:14px;font-family:tahoma,geneva,sans-serif;color:rgb(0,0,0)"><br>
                                            </span></div>
                                        <div>
                                            <font color="#ff0000">
                                                <font face="tahoma, geneva, sans-serif"><span
                                                        style="font-size:14px">&nbsp;</span></font><span
                                                    style="text-decoration-line:none">
                                                    <font face="tahoma, geneva, sans-serif"><span style="font-size:14px">If
                                                            you're interested
                                                            to take a step further and want to be part of the Graphic Era
                                                            University Dehradun, Kindly call our&nbsp;</span></font>
                                                </span>
                                                <font face="tahoma, geneva, sans-serif"><span
                                                        style="font-size:14px">counsellors for further&nbsp;information for
                                                        the same.</span></font>
                                            </font>
                                        </div>
                                    </div>
                                    <div>
                                        <font color="#ff0000">
                                            <font face="tahoma, geneva, sans-serif"><span style="font-size:14px"><br>
                                                </span></font>
                                        </font>
                                    </div>
                                    <div>
                                        <font color="#ff0000" face="tahoma, geneva, sans-serif"><span
                                                style="font-size:14px"><b>For Registration(click)</b>:
                                                <a href="http://bit.ly/2VqM328" target="_blank">bit.ly/2VqM328</a></span>
                                        </font>
                                    </div>
                                    <div>
                                        <font color="#ff0000" face="tahoma, geneva, sans-serif"><br>
                                        </font>
                                    </div>
                                    <div>
                                        <font face="tahoma, geneva, sans-serif" color="#000000"><b>For Assistance Call:
                                                7017974293</b></font>
                                    </div>
                                    <div>
                                        <font color="#ff0000" face="tahoma, geneva, sans-serif"><span
                                                style="font-size:14px">&nbsp;</span></font>
                                    </div>
                                    <div><b>
                                            <font color="#000000">
                                                <font face="inherit">Scholarships&nbsp;</font>available<font face="inherit">
                                                    &nbsp;for&nbsp;</font>
                                            </font><span
                                                style="font-family:inherit">Uttarakhand Domicile, Additional 10% for Girls Students etc.&nbsp;</span>
                                        </b><br>
                                    </div>
                                    <div>
                                        <ul
                                            style="box-sizing:border-box;margin:0px;padding:0px;font-family:&quot;Roboto Condensed&quot;,sans-serif">
                                            <li style="box-sizing:border-box;padding:0px;list-style:none">
                                                <div><br>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div>
                                        <font color="#000000" face="tahoma, geneva, sans-serif"><span
                                                style="font-size:14px">with regards</span></font>
                                    </div>
                                    <div>
                                        <font color="#000000" face="tahoma, geneva, sans-serif"><span
                                                style="font-size:14px">Admission Team</span></font>
                                    </div>
                                    <div>
                                        <font color="#000000" face="tahoma, geneva, sans-serif"><span
                                                style="font-size:14px">Graphic Era University</span></font>
                                    </div>
                                    <div>
                                        <font color="#000000" face="tahoma, geneva, sans-serif"><span
                                                style="font-size:14px">Dehradun</span></font>
                                    </div>
                                    <div>
                                        <font color="#000000" face="tahoma, geneva, sans-serif"><span
                                                style="font-size:14px">Uttarakhand,India</span></font>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        
    </div>
MSG;
if(!empty($_GET['state'])){

if(strtolower($_GET['state']) == "bihar"){
    $strSubject = "Graphic Era University, Dehradun Early Registration Open for 2020 Session";
    $strMessage = <<<MSG
    <div dir="ltr"><div><div id="m_-2451103753467027287m_-8465009755015735174divRplyFwdMsg" dir="ltr"><span style="color:rgb(32,31,30);font-family:tahoma,geneva,sans-serif;font-size:14px">Dear Student,</span><br></div><div><div dir="ltr"><div dir="ltr">
    <div style="margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline">
    <div dir="ltr" style="margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline">
    <div dir="ltr" style="margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline">
    <div style="margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline">
    <div dir="ltr" style="margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline">
    <div dir="ltr" style="margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline">
    <div dir="ltr" style="margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline">
    <div style="margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline">
    <font face="Roboto, Tahoma, Verdana, Segoe, sans-serif" style="font-family:inherit;font-size:inherit;font-style:inherit;font-variant-caps:inherit;color:inherit">&nbsp;</font>
    <div style="margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline">
    <span style="margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline"><span style="margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline"><font face="tahoma, geneva, sans-serif"><span style="font-style:inherit;font-variant-caps:inherit;color:inherit"><b>Graphic
     Era University</b> </span><font color="#0000ff"><span style="font-style:inherit;font-variant-caps:inherit">(<b>NAAC 'A'&nbsp;</b></span><span style="font-size:14px"><b>Grade</b></span><span style="font-style:inherit;font-variant-caps:inherit"><b>&nbsp;&amp;
     NIRF Ranked</b>)</span></font><span style="font-style:inherit;font-variant-caps:inherit;color:inherit">,
    <b>Dehradun</b> announces Registration for 2020 Session:</span></font></span></span></div>
    <div style="font-family:Roboto,Tahoma,Verdana,Segoe,sans-serif;font-size:inherit;font-style:inherit;font-variant-caps:inherit;margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">
    <span style="margin:0px;padding:0px;border:0px;font-family:inherit;font-size:14px;font-style:inherit;font-variant-caps:inherit;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit"><span style="margin:0px;padding:0px;border:0px;font-family:tahoma,geneva,sans-serif;font-size:inherit;font-style:inherit;font-variant-caps:inherit;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit"><br>
    </span></span></div>
    <div style="font-family:inherit;font-size:inherit;font-style:inherit;font-variant-caps:inherit;margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">
    <font face="tahoma, geneva, sans-serif" style="font-size:14px">Due to COVID-19, we understand that you are in a confuse state, as through what is going to happen to admissions</font><font face="Roboto, Tahoma, Verdana, Segoe, sans-serif"><span style="margin:0px;padding:0px;border:0px;font-family:inherit;font-size:14px;font-style:inherit;font-variant-caps:inherit;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">&nbsp;and
     how the admission process will take place for your&nbsp;preferred undergraduate courses.</span></font></div>
    <div style="font-family:inherit;font-size:inherit;font-style:inherit;font-variant-caps:inherit;margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">
    <font face="Roboto, Tahoma, Verdana, Segoe, sans-serif"><span style="margin:0px;padding:0px;border:0px;font-family:inherit;font-size:14px;font-style:inherit;font-variant-caps:inherit;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit"><br>
    </span></font></div>
    <div style="font-family:inherit;font-size:inherit;font-style:inherit;font-variant-caps:inherit;margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">
    <font face="Roboto, Tahoma, Verdana, Segoe, sans-serif"><span style="margin:0px;padding:0px;border:0px;font-family:inherit;font-size:14px;font-style:inherit;font-variant-caps:inherit;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">To
     make it&nbsp;comfortable&nbsp;for you, we are now taking early registrations on the basis of marks you scored in Class XII.&nbsp;</span></font></div>
    <div style="font-family:inherit;font-size:inherit;font-style:inherit;font-variant-caps:inherit;margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">
    <br>
    </div>
    <div style="font-family:inherit;font-size:inherit;font-style:inherit;font-variant-caps:inherit;margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">
    <font color="#000000" face="tahoma, geneva, sans-serif"><span style="margin:0px;padding:0px;border:0px;font-family:inherit;font-size:14px;font-style:inherit;font-variant-caps:inherit;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">The
     undergraduate program in Engineering,Management,<wbr>Commerce,Hospitality &amp; Hotel Management ,Life Sciences,&nbsp;</span><span style="margin:0px;padding:0px;border:0px;font-family:inherit;font-size:14px;font-style:inherit;font-variant-caps:inherit;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">Nursing</span><span style="margin:0px;padding:0px;border:0px;font-family:inherit;font-size:14px;font-style:inherit;font-variant-caps:inherit;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">,Computer
     Applications and more.</span></font></div>
    <div style="font-family:inherit;font-size:inherit;font-style:inherit;font-variant-caps:inherit;margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">
    <span style="margin:0px;padding:0px;border:0px;font-family:tahoma,geneva,sans-serif;font-size:14px;font-style:inherit;font-variant-caps:inherit;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:rgb(0,0,0)"><br>
    </span></div>
    <div style="font-family:inherit;font-size:inherit;font-style:inherit;font-variant-caps:inherit;margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit"><font color="#ff0000"><span style="margin:0px;padding:0px;border:0px;font-family:inherit;font-size:inherit;font-style:inherit;font-variant-caps:inherit;font-stretch:inherit;line-height:inherit;vertical-align:baseline;text-decoration-line:none;color:inherit"><font face="tahoma, geneva, sans-serif"><span style="margin:0px;padding:0px;border:0px;font-family:inherit;font-size:14px;font-style:inherit;font-variant-caps:inherit;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">If
     you're interested to take a step further and want to be part of the Graphic Era University Dehradun, Kindly call our&nbsp;</span></font></span><font face="tahoma, geneva, sans-serif"><span style="margin:0px;padding:0px;border:0px;font-family:inherit;font-size:14px;font-style:inherit;font-variant-caps:inherit;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">counsellors
     for further&nbsp;information for the same.</span></font></font></div>
    </div>
    <div style="font-family:inherit;font-size:inherit;font-style:inherit;font-variant-caps:inherit;margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">
    <font color="#ff0000"><font face="tahoma, geneva, sans-serif"><span style="margin:0px;padding:0px;border:0px;font-family:inherit;font-size:14px;font-style:inherit;font-variant-caps:inherit;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit"><br>
    </span></font></font></div>
    <div style="font-family:inherit;font-size:inherit;font-style:inherit;font-variant-caps:inherit;margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">
    <font color="#ff0000" face="tahoma, geneva, sans-serif"><span style="margin:0px;padding:0px;border:0px;font-family:inherit;font-size:14px;font-style:inherit;font-variant-caps:inherit;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit"><b>For
     Registration(click)</b>:&nbsp;<a href="http://bit.ly/2VqM328" rel="noopener noreferrer" id="m_-2451103753467027287m_-8465009755015735174gmail-LPlnk199558" style="margin:0px;padding:0px;border:0px;font-family:inherit;font-size:inherit;font-style:inherit;font-variant-caps:inherit;font-stretch:inherit;line-height:inherit;vertical-align:baseline" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://bit.ly/2VqM328&amp;source=gmail&amp;ust=1588761065963000&amp;usg=AFQjCNFoPsvfOWFbskX75QbJctnqkLijZg">bit.ly/<wbr>2VqM328</a>&nbsp;</span></font></div>
    <div style="font-family:inherit;font-size:inherit;font-style:inherit;font-variant-caps:inherit;margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">
    <font face="tahoma, geneva, sans-serif" color="#000000"><b>For Assistance Contact: 7017974293</b></font></div>
    <div style="font-family:inherit;font-size:inherit;font-style:inherit;font-variant-caps:inherit;margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">
    <font color="#ff0000" face="tahoma, geneva, sans-serif"><span style="margin:0px;padding:0px;border:0px;font-family:inherit;font-size:14px;font-style:inherit;font-variant-caps:inherit;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">&nbsp;</span></font></div>
    <div style="font-family:inherit;font-size:inherit;font-style:inherit;font-variant-caps:inherit;margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">
    <b><font color="#000000"><font face="inherit">Scholarships&nbsp;</font>available</font><span style="margin:0px;padding:0px;border:0px;font-family:inherit;font-size:inherit;font-style:inherit;font-variant-caps:inherit;font-weight:inherit;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">,
     Additional 10% Scholarship for Girls Students etc.&nbsp;</span>&nbsp;</b><br>
    </div>
    <div style="font-family:inherit;font-size:inherit;font-style:inherit;font-variant-caps:inherit;margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">
    <ul style="box-sizing:border-box;margin:0px;padding:0px;font-family:&quot;Roboto Condensed&quot;,sans-serif">
    <li style="box-sizing:border-box;padding:0px;list-style:none">
    <div style="margin:0px;padding:0px;border:0px;font-family:inherit;font-size:inherit;font-style:inherit;font-variant-caps:inherit;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">
    <br>
    </div>
    </li></ul>
    </div>
    <div style="font-family:inherit;font-size:inherit;font-style:inherit;font-variant-caps:inherit;margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit"><font color="#000000" face="tahoma, geneva, sans-serif"><span style="margin:0px;padding:0px;border:0px;font-family:inherit;font-size:14px;font-style:inherit;font-variant-caps:inherit;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">With
     regards</span></font></div>
    <div style="font-family:inherit;font-size:inherit;font-style:inherit;font-variant-caps:inherit;margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">
    <font color="#000000" face="tahoma, geneva, sans-serif"><span style="margin:0px;padding:0px;border:0px;font-family:inherit;font-size:14px;font-style:inherit;font-variant-caps:inherit;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">Admission
     Team</span></font></div>
    <div style="font-family:inherit;font-size:inherit;font-style:inherit;font-variant-caps:inherit;margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">
    <font color="#000000" face="tahoma, geneva, sans-serif"><span style="margin:0px;padding:0px;border:0px;font-family:inherit;font-size:14px;font-style:inherit;font-variant-caps:inherit;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">Graphic
     Era University</span></font></div>
    <div style="font-family:inherit;font-size:inherit;font-style:inherit;font-variant-caps:inherit;margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">
    <font color="#000000" face="tahoma, geneva, sans-serif"><span style="margin:0px;padding:0px;border:0px;font-family:inherit;font-size:14px;font-style:inherit;font-variant-caps:inherit;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">Dehradun</span></font></div>
    <div style="font-family:inherit;font-size:inherit;font-style:inherit;font-variant-caps:inherit;margin:0px;padding:0px;border:0px;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">
    <font color="#000000" face="tahoma, geneva, sans-serif"><span style="margin:0px;padding:0px;border:0px;font-family:inherit;font-size:14px;font-style:inherit;font-variant-caps:inherit;font-stretch:inherit;line-height:inherit;vertical-align:baseline;color:inherit">Uttarakhand, India</span></font></div><div class="yj6qo"></div>
MSG;
}

}

        $arrTo = array_combine(array_flip($arrTo), $arrTo);
        $arrTo = (array_filter($arrTo));
        $arrSent = [];


        // $arrTo= [];
        // $arrTo[] ="shubhamrastogi1967@gmail.com";
        
        foreach ($arrTo as $recTo) {
            $messageObj =  Yii::$app->mailer->compose()
                ->setFrom(['priyankakumari.admin@geu.ac.in' => "Graphic Era University"])
                ->setTo($recTo)
                // ->setCc('shubhamrastogi1994@hotmail.com')
                ->setSubject($strSubject)

                // ->setTextBody('Plain text content')
                ->setHtmlBody($strMessage);

            if ($messageObj->send()) {
                $handle = fopen($basePath . "/mailsent.csv", "a");
                $line = [$recTo, "Sent",$_GET['state'],date('d-m-Y H:i:s')];
                fputcsv($handle, $line);
                fclose($handle);
                $arrSent[$recTo] = "Sent";
                echo $recTo . '=> Sent<br>';
            } else {
                $handle = fopen($basePath . "/mailsent.csv", "a");
                $line = [$recTo, "Fail",$_GET['state'],date('d-m-Y H:i:s')];
                fputcsv($handle, $line);
                fclose($handle);
                $arrSent[$recTo] = "fail";
                echo $recTo . '=> Fail<br>';
            }
            
        }
        


        // echo '<pre>';
        // print_r($arrSent);

        exit;
        $searchModel = new CountrySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', ['searchModel' => $searchModel, 'dataProvider' => $dataProvider]);
    }

    
}
