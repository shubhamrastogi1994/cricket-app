<?php

namespace backend\controllers;

use backend\models\MatchSeasonSearch;
use backend\models\Team;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * CountryController implements the CRUD actions for Country model.
 */
class PointsController extends BaseController
{

    /**
     * Lists all MatchSeason models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MatchSeasonSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists Points table for the season.
     * @param $intMatchSeasonId
     * @return mixed
     */
    public function actionList($intMatchSeasonId)
    {
        $query = Team::find()
                    ->join('left join','tbl_match as team1','team1.int_team1_id = tbl_team.int_team_id')
                    ->join('left join','tbl_match as team2','team2.int_team2_id = tbl_team.int_team_id')
                    ->andWhere("(`team1`.`int_match_season_id` = {$intMatchSeasonId})
        or (`team2`.`int_match_season_id` = {$intMatchSeasonId})")
                    ->groupBy(Team::tableName().'.int_team_id');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index', compact('dataProvider','intMatchSeasonId'));
    }






}
