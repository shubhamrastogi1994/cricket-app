/**
 * Created by Shubham Rastogi on 30-12-2017.
 */

$(function () {

    $("#globalModal").on("hidden.bs.modal", function (a) {
        $(this).removeData("bs.modal");
        $(this).find(".modal-content").html('<div class="modal-body"><div class="loader text-center"><div class="spinner"><i class="fa fa-spinner fa-pulse fa-5x fa-fw"></i></div></div></div>');
        // $("#globalModal").removeClass("modal-danger").find(".modal-dialog").removeClass("modal-lg modal-sm")
    });
    // $("a[data-target='#globalModal']").unbind();


    $(document).on('click', '[data-target="#globalModal"]', function () {
        if ($('#globalModal').data('bs.modal').isShown) {
            $('#globalModal').find('.modal-content').load($(this).attr('href'));
        } else {
            //if modal isn't open; open it and load content
            $('#globalModal').modal('show').find('.modal-content').load($(this).attr('href'));
        }
    });
});
