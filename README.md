<p align="center">
    <h1 align="center">Cricket App Assignment</h1>
</p>

This project is the repository for the cricket project.

To use this project 

### Note: To install on server skip step after 


### Step 1 
~~~~
git clone https://gitlab.com/shubhamrastogi1994/cricket-app.git project_dir_name
~~~~
### Step 2 :
~~~~
cd project_dir_name
~~~~
### Step 3:
~~~~
php init
~~~~

~~~~

Yii Application Initialization Tool v1.0

Which environment do you want the application to be initialized in?

  [0] Development
  [1] Production

  Your choice [0-1, or "q" to quit] 0

  Initialize the application under 'Development' environment? [yes|no] yes
~~~~~  

### Step 4: 
-- Change Database name in common/config/main-local.php
~~~~
<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=database_name',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
    ],
];

~~~~
### Step 5 :
create database in mysql
~~~~
create database database_name
~~~~
### Step 6:
~~~~
composer update
~~~~
### Step 7:
~~~~
php yii migrate
~~~~

### Step 8: (Local Environment)

hosts file entry 
~~~~
127.0.0.2 cricketapp.com
~~~~

### Step 9: (Local Environment)
 
~~~~
  <VirtualHost 127.0.0.2:80>
  	ServerName cricketapp.com
  	ServerAlias cricketapp.com
	DocumentRoot "${INSTALL_DIR}/www/project_dir_name/backend/web"
	<Directory "${INSTALL_DIR}/www/project_dir_name/backend/web/">
	  Options +Indexes +Includes +FollowSymLinks +MultiViews
	  AllowOverride All
	  Require local
	</Directory>
</VirtualHost>
~~~~


### Step 10 : (Live Environment)

Set Domain name mapping to 
~~~~
DIR_home/project_dir_name/backend/web
~~~~